﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RMS_HIPAA.Framework.Service
{
    public static class RiskAssessmentService
    {
        public static string GenerateRiskId(string FunctionName)
        {
            const string Numbers = "0123456789";
            //string stringFormat = "RSKA*#**#**#*";            

            var stringChars = new char[3];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = Numbers[random.Next(Numbers.Length)];
            }
            var generated = new string(stringChars);

            if(FunctionName == "Administrative")
            {
                return "RSKA" + generated;
            }
            else if(FunctionName == "Physical")
            {
                return "RSKP" + generated;
            }
            else
            {
                return "RSKT" + generated;
            }
        }

        public static string GenerateAssetID()
        {
            const string Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string Numbers = "0123456789";

            var stringChars = new char[3];
            var stringNums = new char[3];
            var random = new Random();
            for (int i = 0; i < stringChars.Length; i++)
            {
                stringNums[i] = Numbers[random.Next(Numbers.Length)];
                stringChars[i]= Alphabets[random.Next(Alphabets.Length)];
            }
            var generated = new string(stringChars) + new string(stringNums);
            return generated;
        }
    }
}
