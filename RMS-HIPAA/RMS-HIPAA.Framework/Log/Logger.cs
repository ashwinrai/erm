﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Appender;
using log4net.Repository;

namespace RMS_HIPAA.Framework.Log
{
    public class Logger
    {
        private static readonly ILog Log;

        static Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
            Log = LogManager.GetLogger("RMS");
        }

        /**
         * Log Debug Messages
         */
        public static void LogDebug(string msg)
        {
            Log.Debug(msg);
        }

        /**
         * Log Info Messages
         */
        public static void LogInfo(string msg)
        {
            Log.Info(msg);
        }

        /**
         * Log Warnings
         */
        public static void LogWarning(string msg)
        {
            Log.Warn(msg);
        }

        /**
		 * Log Error Messages
		 */
        public static void LogError(string msg)
        {
            Log.Error(msg);
        }

        /**
         * Log Exception Messages
         */
        public static void LogError(Exception ex)
        {
            Log.Error(ex.Message, ex);
        }

        /**
		 * Log Exception Messages
		 */
        public static void LogError(string msg, Exception ex)
        {
            Log.Error(msg, ex);
        }

        /**
         * Returns the error log file name
         */
        public static string GetLogFilePath(string logName, string typeName)
        {
            string fileName = null;
            ILoggerRepository rootRep = LogManager.GetRepository();
            foreach (IAppender iApp in rootRep.GetAppenders())
            {
                var app = iApp as FileAppender;
                if (app == null) continue;

                var fApp = app;
                fileName = fApp.File;
                if (iApp.Name.Contains(logName) && iApp.Name.Contains(typeName))
                    break;
            }
            return fileName;
        }
    }
}
