﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Helpers;
using System.Security.Claims;
using System.Web.Security;
using Newtonsoft.Json;
using RMS_HIPAA.Infrastructure.Security;

namespace RMS_HIPAA
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            UnityConfig.RegisterComponents(); //Method call to Complete the Component Registration
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {

            try
            {
                Framework.Log.Logger.LogInfo("auth  cookiee start");

                HttpCookie authCookie = HttpContext.Current.Request.Cookies.Get(FormsAuthentication.FormsCookieName); // Request.Cookies[FormsAuthentication.FormsCookieName];

                if (authCookie != null)
                {
                    Framework.Log.Logger.LogInfo("decrypt cookiee value" + authCookie.Value);
                    try
                    {
                        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                        CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                        CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                        newUser.UserId = serializeModel.UserId;
                        newUser.FirstName = serializeModel.FirstName;
                        newUser.LastName = serializeModel.LastName;
                        newUser.roles = serializeModel.roles;

                        HttpContext.Current.User = newUser;
                    }
                    catch (ArgumentException ex)
                    {
                        FormsAuthentication.SignOut();
                        Framework.Log.Logger.LogInfo("Global error" + ex.Message.ToString());

                    }

                }
            }

            catch (Exception ex1)
            {
                FormsAuthentication.SignOut();
                Framework.Log.Logger.LogInfo("Form Global error" + ex1.Message.ToString());

            }
        }

        protected void Application_End()
        {
            Framework.Log.Logger.LogInfo("Application ended");
        }

        protected void Application_Error()
        {
            var ex = Server.GetLastError();
            Framework.Log.Logger.LogError("Uncaught error in web app ::: Requested URL => " + Request.RawUrl, ex);

            Server.ClearError();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Framework.Log.Logger.LogInfo("Application started");
        }
    }
}
