﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Models.RiskAssessmentModel;

namespace RMS_HIPAA.Reports
{
    public partial class RiskAssessmentListReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string FunctionType = Request.QueryString["FunctionType"];
                string ExportType = Request.QueryString["ExportType"];
                RenderReport(FunctionType, ExportType);
            }
        }

        private void RenderReport(string FunctionType, string ExportType)
        {
            RiskListReportViewer.Reset();
            RiskListReportViewer.LocalReport.EnableExternalImages = true;           

            using (RMSHIPAAEntities db = new RMSHIPAAEntities())
            {
                List<RiskAssessment> scho;
                if (FunctionType == "Physical")
                {
                    scho = db.RiskAssessments.Where(x => x.Risk == "Physical").OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                    List<RiskAssessmentViewModel> RiskList = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                    if(ExportType.ToLower() == "pdf")
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/PhysicalRiskAssessmentPDFList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("PhysicalRiskAssessmentPDFList", RiskList));
                    }
                    else
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/PhysicalRiskAssessmentList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("PhysicalRiskAssessmentList", RiskList));
                    }
                   
                }
                else if (FunctionType == "Administrative")
                {
                    scho = db.RiskAssessments.Where(x => x.Risk == "Administrative").OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                    List<RiskAssessmentViewModel> RiskList = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                    if (ExportType.ToLower() == "pdf")
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/AdministrativeRiskAssessmentPDFList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("AdministrativeRiskAssessmentPDFList", RiskList));
                    }
                    else
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/AdministrativeRiskAssessmentList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("AdministrativeRiskAssessmentList", RiskList));
                    }
                }
                else if (FunctionType == "Technology")
                {
                    scho = db.RiskAssessments.Where(x => x.Risk == "Technology").OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                    List<RiskAssessmentViewModel> RiskList = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                    if (ExportType.ToLower() == "pdf")
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/TechnologyRiskAssessmentPDFList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TechnologyRiskAssessmentPDFList", RiskList));
                    }
                    else
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/TechnologyRiskAssessmentList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("TechnologyRiskAssessmentList", RiskList));
                    }
                }
                else
                {
                    scho = db.RiskAssessments.OrderBy(x => x.Risk).OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                    List<RiskAssessmentViewModel> RiskList = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                    if (ExportType.ToLower() == "pdf")
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RiskAssessmentPDFList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("RiskAssessmentPDFList", RiskList));
                    }
                    else
                    {
                        RiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RiskAssessmentList.rdlc");
                        RiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("RiskAssessmentList", RiskList));
                    }
                }                
            }
        }
    }
}