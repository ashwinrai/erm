﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Models.Reports;

namespace RMS_HIPAA.Reports
{
    public partial class RiskAssessmentKPIListReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string RiskType = Request.QueryString["RiskType"];
                RenderReport(RiskType);
            }
        }

        private void RenderReport(string RiskType)
        {
            RiskKPIListReportViewer.Reset();
            RiskKPIListReportViewer.LocalReport.EnableExternalImages = true;
            List<HighMediumRisk> HighMediumList = new List<HighMediumRisk>();
            List<StatuswiseNumberOfRisk> StatuswiseNumberofRiskList = new List<StatuswiseNumberOfRisk>();
            List<RiskExposureLevelbasedRisk> RiskExposureLevelRisk = new List<RiskExposureLevelbasedRisk>();
            List<PercentageOfRiskImpactCategory> PercentageofRiskImpactCategoryList = new List<PercentageOfRiskImpactCategory>();
            List<PercentageOfRiskImpactLevel> PercentageofRislImpactLevelList = new List<PercentageOfRiskImpactLevel>();
            List<QuarterwiseNoOfRiskTarget> QuarterwiseNoofRiskTargetList = new List<QuarterwiseNoOfRiskTarget>();

            using (RMSHIPAAEntities db = new RMSHIPAAEntities())
            {
                var query = from ara in db.RiskAssessments
                            group ara by ara.CreatedOn.Year into ara
                            select new
                            {
                                Year = ara.Key
                            };
               
                int high, medium;
               
                foreach (var ara in query)
                {
                   
                    high = (from h in db.RiskAssessments where h.CreatedOn.Year == ara.Year && h.RiskExposureLevel == "High" && h.Risk == RiskType && h.Approval == true && h.Status != null && h.Status != "Close" select h).Count();
                    medium = (from m in db.RiskAssessments where m.CreatedOn.Year == ara.Year && m.RiskExposureLevel == "Medium" && m.Risk == RiskType && m.Approval == true && m.Status != null && m.Status != "Close" select m).Count();
                    HighMediumRisk HR = new HighMediumRisk();
                    HR.Year = ara.Year.ToString();
                    HR.MediumRisk = medium;
                    HR.HighRisk = high;
                    HighMediumList.Add(HR);
                }


                var query1 = db.RiskAssessments.Where(r => r.Risk == RiskType && r.Approval == true && r.Status != null).GroupBy(ara => ara.Status).Select(d => new
                {
                    Stat = d.Key,
                    RiskAssessment = d.Count()
                });              
           
                foreach (var ar in query1)
                {                  
                    string sts = ar.Stat ?? "Unassigned";
                    StatuswiseNumberOfRisk SNR = new StatuswiseNumberOfRisk();
                    SNR.Status = sts;
                    SNR.NumberofRisks = ar.RiskAssessment;
                    StatuswiseNumberofRiskList.Add(SNR);
                }

                var query2 = db.RiskAssessments.Where(r => r.Risk == RiskType && r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskExposureLevel != null).GroupBy(ara => ara.RiskExposureLevel).Select(d => new
                {
                    Stat = d.Key,
                    RiskAssessment = d.Count()
                });           
                
                foreach (var ar in query2)
                {                  
                    string sts = ar.Stat ?? "Unassigned";
                    RiskExposureLevelbasedRisk RELRisk = new RiskExposureLevelbasedRisk();
                    RELRisk.RiskExposure = sts;
                    RELRisk.NumberofRisks = ar.RiskAssessment;
                    RiskExposureLevelRisk.Add(RELRisk);               
                }

                var query3 = db.RiskAssessments.Where(r => r.Risk == RiskType && r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
                {
                    Category = d.Key,
                    RiskAssessment = d.Count()
                });
              
                foreach (var ar in query3)
                {
                    string sts = ar.Category ?? "Unassigned";
                    PercentageOfRiskImpactCategory PRIC = new PercentageOfRiskImpactCategory();
                    PRIC.ImpactCategory = sts;
                    PRIC.NumberofRisks = ar.RiskAssessment;                   
                    PercentageofRiskImpactCategoryList.Add(PRIC);
                }

                var query4 = db.RiskAssessments.Where(r => r.Risk == RiskType && r.Approval == true && r.Status != null && r.Status != "Close" && r.ImpactLevel != null).GroupBy(ara => ara.ImpactLevel).Select(d => new
                {
                    Il = d.Key,
                    RiskAssessment = d.Count()
                });               
               
                foreach (var ar in query4)
                {                  
                    string sts = ar.Il ?? "Unassigned";
                    PercentageOfRiskImpactLevel PRIL = new PercentageOfRiskImpactLevel();
                    PRIL.ImpactLevel = sts;
                    PRIL.NumberofRisks = ar.RiskAssessment;
                    PercentageofRislImpactLevelList.Add(PRIL);
                }

                var query5 = db.RiskAssessments.Where(r => r.Risk == RiskType && r.Approval == true && r.Status != null && r.Status != "Close" && r.TargetQuarter != null).GroupBy(ara => ara.TargetQuarter).Select(d => new
                {
                    Tq = d.Key,
                    RiskAssessment = d.Count()
                });
               
                foreach (var ar in query5)
                {
                    string sts = ar.Tq ?? "Unassigned";
                    QuarterwiseNoOfRiskTarget QNR = new QuarterwiseNoOfRiskTarget();
                    QNR.Quarter = sts;
                    QNR.NumberofRisks = ar.RiskAssessment;
                    QuarterwiseNoofRiskTargetList.Add(QNR);
                }

                RiskKPIListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RiskAssessmentKPIPDFList.rdlc");
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartHighMedium", HighMediumList));
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartStatusWiseNoOfRisk", StatuswiseNumberofRiskList));
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartRiskExposureLevelbasedRisk", RiskExposureLevelRisk));
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartPercentageofRiskImpactCategory", PercentageofRiskImpactCategoryList));
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartPercentageofRiskImpactLevel", PercentageofRislImpactLevelList));
                RiskKPIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KPIChartQuarterwiseNoofRiskTarget", QuarterwiseNoofRiskTargetList));
            }
        }
   }
}