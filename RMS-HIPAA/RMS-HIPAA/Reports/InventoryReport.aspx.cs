﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Models.AssetInventoryModel;

namespace RMS_HIPAA.Reports
{
    public partial class SoftwareInventoryReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string InventoryType = Request.QueryString["InventoryType"];
                string ExportType = Request.QueryString["ExportType"];
                RenderReport(InventoryType,ExportType);
            }
        }
        private void RenderReport(string InventoryType,string ExportType)
        {
            InventoryReportViewer.Reset();
            InventoryReportViewer.LocalReport.EnableExternalImages = true;
            using (RMSHIPAAEntities db = new RMSHIPAAEntities())
            {
                if(InventoryType== "Software")
                {
                    List<SoftwareInventory> scho;
                    scho = db.SoftwareInventories.ToList();
                    List<SoftwareInventoryViewModel> SWIList = scho.Select(x => new SoftwareInventoryViewModel(x)).ToList();
                    if (ExportType.ToLower() == "pdf")
                    {
                        InventoryReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/SoftwareAssetInventoryPDF.rdlc");
                        InventoryReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("SoftwareAssetInventoryPDF", SWIList));
                    }
                    else
                    {
                        //SoftwareInventoryReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/PhysicalRiskAssessmentList.rdlc");
                        //SoftwareInventoryReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("PhysicalRiskAssessmentList", RiskList));
                    }
                }
                else
                {
                    List<HardwareInventory> scho;
                    scho = db.HardwareInventories.ToList();
                    List<HardwareInventoryViewModel> HWIList = scho.Select(x => new HardwareInventoryViewModel(x)).ToList();
                    if (ExportType.ToLower() == "pdf")
                    {
                        InventoryReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/HardwareAssetInventoryPDF.rdlc");
                        InventoryReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("HardwareAssetInventoryPDF", HWIList));
                    }
                    else
                    {
                        //SoftwareInventoryReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/PhysicalRiskAssessmentList.rdlc");
                        //SoftwareInventoryReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("PhysicalRiskAssessmentList", RiskList));
                    }
                }
                
            }

        }
    }
}