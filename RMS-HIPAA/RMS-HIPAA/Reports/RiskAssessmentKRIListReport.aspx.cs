﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Models.Reports;

namespace RMS_HIPAA.Reports
{
    public partial class RiskAssessmentKRIListReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RenderReport();
            }
        }

        private void RenderReport()
        {
            RiskKRIListReportViewer.Reset();
            RiskKRIListReportViewer.LocalReport.EnableExternalImages = true;
            List<PercentageImpactCategory> PercentageImpactCategoryList = new List<PercentageImpactCategory>();
            List<ImpactCategoryRiskData> ImpactCategoryRiskList = new List<ImpactCategoryRiskData>();
            List<TargetQuarterRiskData> TargetQuarterRiskList = new List<TargetQuarterRiskData>();

            using (RMSHIPAAEntities db = new RMSHIPAAEntities())
            {
                var query = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
                {
                    Category = d.Key,
                    RiskAssessment = d.Count()
                });
                var total = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).Count();


                foreach (var ar in query)
                {
                    ImpactCategoryRiskData ICR = new ImpactCategoryRiskData();
                    string sts = ar.Category ?? "Unassigned";
                    var per = ((double)ar.RiskAssessment / total) * 100;
                    ICR.ImpactCategory = sts;
                    ICR.NumberofRisks = ar.RiskAssessment;
                    ImpactCategoryRiskList.Add(ICR);
                }

                var query1 = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
                {
                    Category = d.Key,
                    RiskAssessment = d.Count()
                });
                var total1 = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).Count();

                foreach (var ar in query1)
                {
                    PercentageImpactCategory PIC = new PercentageImpactCategory();
                    string sts = ar.Category ?? "Unassigned";
                    var per = ((double)ar.RiskAssessment / total1) * 100;
                    PIC.ImpactCategory = sts;
                    PIC.PercentageofRisk = Math.Round(per, 1);
                    PercentageImpactCategoryList.Add(PIC);
                }

                var query5 = db.RiskAssessments.Where(r => r.Risk == "Physical" && r.Approval == true && r.Status != null && r.Status != "Close" && r.TargetQuarter != null).GroupBy(ara => ara.TargetQuarter).Select(d => new
                {
                    Tq = d.Key,
                    RiskAssessment = d.Count()
                });

                foreach (var ar in query5)
                {
                    TargetQuarterRiskData TQR = new TargetQuarterRiskData();
                    string sts = ar.Tq ?? "Unassigned";
                    TQR.Quarter = sts;
                    TQR.NumberofRisks = ar.RiskAssessment;
                    TargetQuarterRiskList.Add(TQR);
                }

                RiskKRIListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/RiskAssessmentKRIPDFList.rdlc");
                RiskKRIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KRIImpactCategory", ImpactCategoryRiskList));
                RiskKRIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KRIPercentageImpactCategoryList", PercentageImpactCategoryList));
                RiskKRIListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("KRITargetQuarterList", TargetQuarterRiskList));
            }

        }
    }
}