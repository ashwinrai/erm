﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using System.Reflection;

namespace RMS_HIPAA.Reports
{
    public partial class SelfRiskAssessmentListReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string FunctionType = Request.QueryString["FunctionType"];
                string FromDate = Request.QueryString["FromDate"];
               // string ToDate = Request.QueryString["ToDate"];
                RenderReport(FunctionType, FromDate);
            }
        }

        private void RenderReport(string FunctionType, string FromDate)
        {
            SelfRiskListReportViewer.Reset();
            SelfRiskListReportViewer.LocalReport.EnableExternalImages = true;
           
          
            using (RMSHIPAAEntities db = new RMSHIPAAEntities())
            {
                SelfAssessmentQuestionTestViewModel scho = new SelfAssessmentQuestionTestViewModel();                


                    List<SelfAssessmentQuestionTestDetailsViewModelReport> SQTList = scho.ConvertToQuestionTestViewReport(FunctionType, FromDate);

                    SelfRiskListReportViewer.LocalReport.ReportPath = Server.MapPath("~/Reports/SelfRiskAssessmentReport.rdlc");
                    SelfRiskListReportViewer.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("AdministrativeSelfRiskAssessmentList", SQTList));
      
            }
        }
    }
}