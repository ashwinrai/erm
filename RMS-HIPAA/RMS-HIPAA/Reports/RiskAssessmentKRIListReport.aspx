﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RiskAssessmentKRIListReport.aspx.cs" Inherits="RMS_HIPAA.Reports.RiskAssessmentKRIListReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
   <form id="form1" runat="server">
    <div>
      <asp:ScriptManager runat="server"></asp:ScriptManager>
        <rsweb:ReportViewer ID="RiskKRIListReportViewer"  runat="server" Width="100%" PageCountMode ="Actual" BackColor="Gray"  BorderColor="WhiteSmoke" AsyncRendering="false" SizeToReportContent="true" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" ForeColor="White" InternalBorderColor="#333333" LinkActiveColor="White" LinkDisabledColor="White" Font-Overline="False" LinkActiveHoverColor="REd"></rsweb:ReportViewer>       
    </div>
    </form>
</body>
</html>
