﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Models.Reports
{
    public class ReportViewModel
    {

    }

    public class HighMediumRisk
    {
        public string Year { get; set; }
        public int MediumRisk { get; set; }
        public int HighRisk { get; set; }
    }
    public class StatuswiseNumberOfRisk
    {
        public string Status { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class RiskExposureLevelbasedRisk
    {
        public string RiskExposure { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class PercentageOfRiskImpactCategory
    {
        public string ImpactCategory { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class PercentageOfRiskImpactLevel
    {
        public string ImpactLevel { get; set; }
        public int NumberofRisks { get; set; }
    }
        
    public class QuarterwiseNoOfRiskTarget
    {
        public string Quarter { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class PercentageImpactCategory
    {
        public string ImpactCategory { get; set; }
        public double PercentageofRisk { get; set; }
    }

    public class ImpactCategoryRiskData
    {
        public string ImpactCategory { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class TargetQuarterRiskData
    {
        public string Quarter { get; set; }
        public int NumberofRisks { get; set; }
    }

    public class SelfRiskAssessmentData
    {
        public int SINo { get; set; }
        public string Questions { get; set; }
        public Boolean ResponseYes { get; set; }
        public Boolean ResponseNo { get; set; }
        public Boolean ResponseNA { get; set; }
        public string Comments { get; set; }
    }

}