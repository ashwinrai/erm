﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web.Mvc;
using RMS_HIPAA.DAL;
using RMS_HIPAA.Framework.Service;
using System.Linq;
using System.Data.Entity;

namespace RMS_HIPAA.Models.RiskAssessmentModel
{
    public class RiskAssessmentViewModel
    {
        public Guid RiskAssessmentId { get; set; }

        [Display(Name = "Risk Id")]
        public string RiskId { get; set; }

        public string Risk { get; set; }

        [Display(Name = "Risk Agent")]
        public string RiskAgent { get; set; }
        public SelectList RiskAgentList { get; private set; }

        [Display(Name = "Entity/Asset Name")]
        public string AssetName { get; set; }

        [Display(Name = "Department/Function Name")]
        public string DepartmentName { get; set; }

        [Display(Name = "Assessed Date")]
        public DateTime? AssessedDate { get; set; }
        public string AssessedDateVal { get; set; }

        public string StrAssessedDate { get; set; }

        [Display(Name = "Risk Description")]
        public string RiskDescription { get; set; }

        [Display(Name = "Assessor/Owner")]
        public string Owner { get; set; }

        [Display(Name = "Threat/Vulnerability Description ")]
        public string ThreatVulnerabilityDescription { get; set; }

        [Display(Name = "Risk Input")]
        public string RiskInput { get; set; }
        public SelectList RiskInputList { get; private set; }

        [Display(Name = "Threat Source")]
        public string ThreatSource { get; set; }
        public SelectList ThreatSourceList { get; private set; }

        [Display(Name = "Risk Impact Category")]
        public string RiskImpactCategory { get; set; }
        public SelectList RiskImpactCategoryList { get; private set; }

        [Display(Name = "Probability of Occurance")]
        public string ProbabilityofOccurence { get; set; }
        public SelectList ProbabilityofOccurenceList { get; private set; }
        public string ProbabilityofOccurenceValue { get; set; }

        [Display(Name = "Impact Level")]
        public string ImpactLevel { get; set; }
        public SelectList ImpactLevelList { get; private set; }
        public string ImpactLevelValue { get; set; }

        [Display(Name = "Risk Exposure Level")]
        public string RiskExposureLevel { get; set; }

        [Display(Name = "Existing Controls ")]
        public string ExistingControls { get; set; }

        [Display(Name = "New / Proposed Controls ")]
        public string ActionProposed { get; set; }

        [Display(Name = "Responsible")]
        public string Responsible { get; set; }

        [Display(Name = "Target Date")]
        public DateTime? TargetDate { get; set; }
        public string TargetDateVal { get; set; }

        public string strTargetDate { get; set; }

        [Display(Name = "Target Quarter")]
        public string TargetQurter { get; set; }
        public SelectList TargetQurterList { get; private set; }

        [Display(Name = "Responsible Dept")]
        public string ResponsibleDepartment { get; set; }
        public SelectList ResponsibleDepartmentList { get; private set; }

        [Display(Name = "Risk Response")]
        public string RiskResponse { get; set; }
        public SelectList RiskResponseList { get; private set; }

        [Display(Name = "Status")]
        public string Status { get; set; }
        public SelectList StatusList { get; private set; }

        [Display(Name = " Closed Date")]
        public DateTime? ClosedDate { get; set; }
        public string ClosedDateVal { get; set; }

    public string strClosedDate { get; set; }

        public bool SentToApproval { get; set; }
        public bool Approval { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedOn { get; set; }
        public string strApprovedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public string performPIR { get; set; }
        [Display(Name = "Post implement Impact Level")]
        public string PIRImpactLevel { get; set; }
        public string PIRImpactLevelValue { get; set; }

        [Display(Name = "Post implement Probability of Occurence")]
        public string PIRProbabilityOfOccurence { get; set; }
        public string PIRProbabilityofOccurenceValue { get; set; }

        [Display(Name = "Post implement Risk exposure level")]
        public string PIRRiskExposureLevel { get; set; }
        [Display(Name = "Post implement Comments")]
        public string PIRComments { get; set; }
        public DateTime? PIRCreatedOn { get; set; }
        public string PIRCreatedBy { get; set; }
        public string PIRModifiedBy { get; set; }
        public DateTime? PIRModifiedOn { get; set; }


        public RiskAssessmentViewModel()    
        {

        }

        public async Task InitializeValues(string FunctionName)
        {
            RiskId = RiskAssessmentService.GenerateRiskId(FunctionName);
            Risk = FunctionName;
            AssessedDate = DateTime.UtcNow;
            await PopulateListData(FunctionName, null, null, null, null, null, null, null, null, null, null);
        }


        private async Task PopulateListData(string FunctionName, string riskAgentID, string riskInputID, string riskImpactCategoryID, string probailityOccurenceID, string threatSourceID, string impactLevelID, string targetQuarterID, string responsibleDeptID, string riskResponseID, string statusID)
        {
            try
            {
                using (var db = new RMSHIPAAEntities())
                {
                    var riskAgents = await db.RiskAgents.Where(x=>x.Risk == FunctionName).OrderBy(x => x.Name).ToListAsync();
                    RiskAgentList = new SelectList(riskAgents, "Name", "Name", riskAgentID);

                    var riskInputs = new List<KeyValuePair<string, string>>
                        {                           
                            new KeyValuePair<string, string>("Feedback", "Feedback"),
                            new KeyValuePair<string, string>("Audit", "Audit"),
                            new KeyValuePair<string, string>("Management Review","Management Review"),
                            new KeyValuePair<string, string>("Periodic Review","Periodic Review"),
                            new KeyValuePair<string, string>("Customer","Customer")
                        };
                    RiskInputList = new SelectList(riskInputs, "Key", "Value", riskInputID);

                    var riskImpactCategorys = new List<KeyValuePair<string, string>>
                        {                            
                            new KeyValuePair<string, string>("Integrity", "Integrity"),
                            new KeyValuePair<string, string>("Confidentiality", "Confidentiality"),
                            new KeyValuePair<string, string>("Availability", "Availability")
                        };
                    RiskImpactCategoryList = new SelectList(riskImpactCategorys, "Key", "Value", riskImpactCategoryID);

                    var probailityOccurences = await db.ProbabilityofOccurences.OrderByDescending(x => x.ProbabilityValue).ToListAsync();
                    ProbabilityofOccurenceList = new SelectList(probailityOccurences, "ProbabilityValue", "Name", probailityOccurenceID);


                    var threatSources = new List<KeyValuePair<string, string>>
                        {                          
                            new KeyValuePair<string, string>("Human", "Human"),
                            new KeyValuePair<string, string>("Natural", "Natural")                        
                        };
                    ThreatSourceList = new SelectList(threatSources, "Key", "Value", threatSourceID);                  


                    var impactLevels = await db.ImpactLevels.OrderByDescending(x=>x.ImpactValue).ToListAsync();
                    ImpactLevelList = new SelectList(impactLevels, "ImpactValue", "Name", impactLevelID);                

                    var targetQuarters = new List<KeyValuePair<string, string>>
                        {                          
                            new KeyValuePair<string, string>("Q1", "Q1"),
                            new KeyValuePair<string, string>("Q2", "Q2"),
                            new KeyValuePair<string, string>("Q3", "Q3"),
                            new KeyValuePair<string, string>("Q4", "Q4")
                        };
                    TargetQurterList = new SelectList(targetQuarters, "Key", "Value", targetQuarterID);


                    var responsibleDepts = await db.Departments.ToListAsync();
                    ResponsibleDepartmentList = new SelectList(responsibleDepts, "Name", "Name", responsibleDeptID);

                    var riskReponses = new List<KeyValuePair<string, string>>
                        {                            
                            new KeyValuePair<string, string>("Transfer", "Transfer"),
                            new KeyValuePair<string, string>("Mitigate", "Mitigate"),
                            new KeyValuePair<string, string>("Avoid", "Avoid"),
                            new KeyValuePair<string, string>("Accept", "Accept")
                        };
                    RiskResponseList = new SelectList(riskReponses, "Key", "Value", riskResponseID);

                    var stauses = new List<KeyValuePair<string, string>>
                        {                           
                            new KeyValuePair<string, string>("Open", "Open"),
                            new KeyValuePair<string, string>("In-Progress", "In-Progress"),
                            new KeyValuePair<string, string>("Close", "Close")
                        };
                    StatusList = new SelectList(stauses, "Key", "Value", statusID);

                }
            }
            catch (Exception ex)
            {
                RMS_HIPAA.Framework.Log.Logger.LogError(ex);
            }


        }

        public DAL.RiskAssessment ConvertToRiskAssessment()
        {
             var tech = new DAL.RiskAssessment
            {
                Id = Guid.NewGuid(),
                RiskId = RiskId,
                Risk = Risk,
                RiskAgent = RiskAgent,
                AssetName = AssetName,
                DepartmentorFunctionName = DepartmentName,
                AssessedDate = AssessedDate,
                RiskDescription = RiskDescription,
                AssessorOwner = Owner,
                ThreatVulnerabilityDescription = ThreatVulnerabilityDescription,
                RiskInput = RiskInput,
                ThreatSource = ThreatSource,
                RiskImpactCategory = RiskImpactCategory,
                ProbabilityofOccurence =ProbabilityofOccurenceValue,//"High",
                RiskExposureLevel  = RiskExposureLevel, //"Medium",//
                ImpactLevel = ImpactLevelValue,//"Medium",//
                ExistingControl=ExistingControls,
                NewProposedControl = ActionProposed,
                Responsible = Responsible,
                TargetDateImplement = TargetDate,
                TargetQuarter = TargetQurter,
                Department = ResponsibleDepartment,
                RiskResponse = RiskResponse,
                Status = Status,
                ClosedDate = ClosedDate,
                SentToApproval = SentToApproval,
                Approval = Approval,
                CreatedBy = "Indidge",
                CreatedOn = DateTime.Now           
            };
            return tech;
        }

        public RiskAssessmentViewModel ConvertFromRiskAssessment(DAL.RiskAssessment ARisk)
        {
            RiskAssessmentId = ARisk.Id;
            DepartmentName = ARisk.DepartmentorFunctionName;
            RiskId = ARisk.RiskId;
            Risk = ARisk.Risk;
            RiskAgent = ARisk.RiskAgent;
            AssetName = ARisk.AssetName;
            AssessedDate = ARisk.AssessedDate;
            StrAssessedDate = (ARisk.AssessedDate != null) ? ARisk.AssessedDate.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            RiskDescription = ARisk.RiskDescription;
            Owner = ARisk.AssessorOwner;
            RiskInput = ARisk.RiskInput;
            ThreatVulnerabilityDescription = ARisk.ThreatVulnerabilityDescription;
            RiskImpactCategory = ARisk.RiskImpactCategory;
            ProbabilityofOccurence = ARisk.ProbabilityofOccurence;
            ThreatSource = ARisk.ThreatSource;
            ImpactLevel = ARisk.ImpactLevel;
            RiskExposureLevel = ARisk.RiskExposureLevel;
            ExistingControls = ARisk.ExistingControl;
            ActionProposed = ARisk.NewProposedControl;
            Responsible = ARisk.Responsible;
            TargetDate = ARisk.TargetDateImplement;
            strTargetDate = (ARisk.TargetDateImplement != null) ? ARisk.TargetDateImplement.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            TargetQurter = ARisk.TargetQuarter;
            ResponsibleDepartment = ARisk.Department;
            RiskResponse = ARisk.RiskResponse;
            Status = ARisk.Status;
            ClosedDate = ARisk.ClosedDate;
            strClosedDate = (ARisk.ClosedDate != null) ? ARisk.ClosedDate.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            Approval = ARisk.Approval;
            SentToApproval = ARisk.SentToApproval;
            strApprovedOn = (ARisk.ApprovedOn != null) ? ARisk.ApprovedOn.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            CreatedBy = ARisk.CreatedBy;
            CreatedOn = ARisk.CreatedOn;
            ModifiedBy = ARisk.ModifiedBy;
            ModifiedOn = ARisk.ModifiedOn;
            PIRProbabilityOfOccurence = ARisk.PIRProbabilityOfOccurence;
            PIRImpactLevel = ARisk.PIRImpactLevel;
            PIRRiskExposureLevel = ARisk.PIRRiskExposureLevel;
            PIRComments = ARisk.PIRComments;
            PIRCreatedBy = ARisk.PIRCreatedBy;
            PIRCreatedOn = ARisk.PIRCreatedOn;
            PIRModifiedBy = ARisk.PIRModifiedBy;
            PIRModifiedOn = ARisk.PIRModifiedOn;
            return this;
        }

        public RiskAssessmentViewModel(DAL.RiskAssessment ARisk)
        {
            RiskAssessmentId = ARisk.Id;
            DepartmentName = ARisk.DepartmentorFunctionName;
            RiskId = ARisk.RiskId;
            Risk = ARisk.Risk;
            RiskAgent = ARisk.RiskAgent;
            AssetName = ARisk.AssetName;
            AssessedDate = ARisk.AssessedDate;
            StrAssessedDate = (ARisk.AssessedDate != null) ? ARisk.AssessedDate.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            RiskDescription = ARisk.RiskDescription;
            Owner = ARisk.AssessorOwner;
            RiskInput = ARisk.RiskInput;
            ThreatVulnerabilityDescription = ARisk.ThreatVulnerabilityDescription;
            RiskImpactCategory = ARisk.RiskImpactCategory;
            ProbabilityofOccurence = ARisk.ProbabilityofOccurence;
            ThreatSource = ARisk.ThreatSource;
            ImpactLevel = ARisk.ImpactLevel;
            RiskExposureLevel = ARisk.RiskExposureLevel;
            ExistingControls = ARisk.ExistingControl;
            ActionProposed = ARisk.NewProposedControl;
            Responsible = ARisk.Responsible;
            TargetDate = ARisk.TargetDateImplement;
            strTargetDate = (ARisk.TargetDateImplement != null) ? ARisk.TargetDateImplement.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            TargetQurter = ARisk.TargetQuarter;
            ResponsibleDepartment = ARisk.Department;
            RiskResponse = ARisk.RiskResponse;
            Status = ARisk.Status;
            ClosedDate = ARisk.ClosedDate;
            strClosedDate = (ARisk.ClosedDate != null) ? ARisk.ClosedDate.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            Approval = ARisk.Approval;
            SentToApproval = ARisk.SentToApproval;
            CreatedBy = ARisk.CreatedBy;
            CreatedOn = ARisk.CreatedOn;
            ModifiedBy = ARisk.ModifiedBy;
            ModifiedOn = ARisk.ModifiedOn;
            ApprovedOn = ARisk.ApprovedOn;
            strApprovedOn = (ARisk.ApprovedOn != null) ? ARisk.ApprovedOn.Value.ToString("MM/dd/yyyy").Replace('-', '/') : "";
            ApprovedBy = ARisk.ApprovedBy;

            PIRProbabilityOfOccurence = ARisk.PIRProbabilityOfOccurence;
            PIRImpactLevel = ARisk.PIRImpactLevel;
            PIRRiskExposureLevel = ARisk.PIRRiskExposureLevel;
            PIRComments = ARisk.PIRComments;
            PIRCreatedBy = ARisk.PIRCreatedBy;
            PIRCreatedOn = ARisk.PIRCreatedOn;
            PIRModifiedBy = ARisk.PIRModifiedBy;
            PIRModifiedOn = ARisk.PIRModifiedOn;
        }
    }
}