﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Models.SelfRiskAssessmentModel
{
    public class SelfRiskAssessmentQuestionsViewModel
    {
        public int Id { get; set; }

        public int SINo { get; set; }

        public string Questions { get; set; }

        public string RiskType { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public SelfRiskAssessmentQuestionsViewModel()
        {

        }

        public DAL.SelfRiskAssessmentQuestion ConvertToSelfRiskAssessmentQuestions()
        {
            var riskquest = new DAL.SelfRiskAssessmentQuestion
            {
                Id = Id,
                Question = Questions,
                RiskType = RiskType,
                CreatedBy = "Indidge",
                CreatedOn = DateTime.Now
            };
            return riskquest;
        }

        public SelfRiskAssessmentQuestionsViewModel ConvertFromSelfRiskAssessmentQuestions(DAL.SelfRiskAssessmentQuestion sRisk)
        {
            Id = sRisk.Id;
            Questions = sRisk.Question;
            RiskType = sRisk.RiskType;
            CreatedBy = sRisk.CreatedBy;
            CreatedOn = sRisk.CreatedOn;
            ModifiedBy = sRisk.ModifiedBy;
            ModifiedOn = sRisk.ModifiedOn;

            return this;
        }

        public SelfRiskAssessmentQuestionsViewModel(DAL.SelfRiskAssessmentQuestion sRisk, int j)
        {
            Id = sRisk.Id;
            Questions = sRisk.Question;
            SINo = j;
            RiskType = sRisk.RiskType;
            CreatedBy = sRisk.CreatedBy;
            CreatedOn = sRisk.CreatedOn;
            ModifiedBy = sRisk.ModifiedBy;
            ModifiedOn = sRisk.ModifiedOn;
        }

    }
}