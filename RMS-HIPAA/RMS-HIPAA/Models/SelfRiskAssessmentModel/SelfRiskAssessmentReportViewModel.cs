﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Models.SelfRiskAssessmentModel
{
    public class SelfRiskAssessmentReportViewModel
    {
        [Display(Name = "Select a Date")]
        public string FromDate { get; set; }
    }

}