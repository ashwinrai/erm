﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Models.SelfRiskAssessmentModel
{
    public class SelfAssessmentQuestionTestDetailsViewModel
    {
        public int Id { get; set; }
        public int TestRefId { get; set; }
        public int SINo { get; set; }
        public int QuestionId { get; set; }
        public string QuestionName { get; set; }
        public string Response { get; set; }
        public string Comments { get; set; }
    }

    public class SelfAssessmentQuestionTestDetailsViewModelReport
    {
        public string AssessorName { get; set; }
        public DateTime AssessedDate { get; set; }
        public int SINo { get; set; }        
        public string Questions { get; set; }
        public string ResponseStatus { get; set; }      
        public string Comments { get; set; }
    }
}