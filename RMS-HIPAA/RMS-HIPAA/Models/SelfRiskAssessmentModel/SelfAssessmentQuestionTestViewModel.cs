﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.DAL;
using System.Threading.Tasks;
using System.Globalization;

namespace RMS_HIPAA.Models.SelfRiskAssessmentModel
{
    public class SelfAssessmentQuestionTestViewModel
    {
        public int Id { get; set; }
        public string AssessorName { get; set; }
        public DateTime AssessedDate { get; set; }
        public string RiskType { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public List<SelfAssessmentQuestionTestDetailsViewModel> QuestionViewList { get; set; }

        public SelfAssessmentQuestionTestViewModel()
        {
            this.QuestionViewList = new List<SelfAssessmentQuestionTestDetailsViewModel>();
        }

        public SelfAssessmentQuestionTestViewModel ConvertToQuestionView(string accesseddate, string FunctionType)
        {
            DateTime dt = DateTime.ParseExact(accesseddate, "MM/dd/yyyy", CultureInfo.CurrentCulture);

            SelfAssessmentQuestionTestViewModel SQTview = new SelfAssessmentQuestionTestViewModel();
            using (var db = new RMSHIPAAEntities())
            {
                var count = db.SelfRiskAssessmentTests.Where(x => x.AssessedDate >= dt && x.AssessedDate <= dt && x.RiskType == FunctionType).Count();
                if (count > 0)
                {
                    SelfRiskAssessmentTest SRT = db.SelfRiskAssessmentTests.Where(x => x.AssessedDate >= dt && x.AssessedDate <= dt && x.RiskType == FunctionType).SingleOrDefault();
                    SQTview.AssessorName = SRT.AssessorName;
                    SQTview.AssessedDate = SRT.AssessedDate;
                    SQTview.RiskType = SRT.RiskType;
                    SQTview.Id = SRT.Id;
                    List<SelfRiskAssessmentTestDetail> SRTDetails = db.SelfRiskAssessmentTestDetails.Where(x => x.TestRefId == SRT.Id).ToList();
                    int j = 1;
                    foreach (SelfRiskAssessmentTestDetail SR in SRTDetails)
                    {
                        SelfAssessmentQuestionTestDetailsViewModel SQDetailViewModel = new SelfAssessmentQuestionTestDetailsViewModel();
                        SQDetailViewModel.TestRefId = SR.TestRefId;
                        SQDetailViewModel.SINo = j;
                        SQDetailViewModel.QuestionId = SR.QuestionId;
                        SQDetailViewModel.QuestionName = SR.SelfRiskAssessmentQuestion.Question;
                        SQDetailViewModel.Response = SR.Response;
                        SQDetailViewModel.Comments = SR.Comments;
                        SQTview.QuestionViewList.Add(SQDetailViewModel);
                        j = j + 1;
                    }
                }
                else
                {
                    var OldDataCount = db.SelfRiskAssessmentTests.Where(x=> x.RiskType == FunctionType).Count();
                    if (OldDataCount > 0)
                    {
                        SelfRiskAssessmentTest SRT = db.SelfRiskAssessmentTests.Where(x => x.RiskType == FunctionType).OrderByDescending(y => y.Id).FirstOrDefault();
                        SQTview.AssessorName = SRT.AssessorName;
                        SQTview.AssessedDate = DateTime.Now;
                        SQTview.RiskType = FunctionType;
                        SQTview.Id = 0;

                        List<SelfRiskAssessmentTestDetail> SRTDetails = db.SelfRiskAssessmentTestDetails.Where(x => x.TestRefId == SRT.Id).ToList();
                        int j = 1;
                        foreach (SelfRiskAssessmentTestDetail SR in SRTDetails)
                        {
                            SelfAssessmentQuestionTestDetailsViewModel SQDetailViewModel = new SelfAssessmentQuestionTestDetailsViewModel();
                            SQDetailViewModel.TestRefId = 0;
                            SQDetailViewModel.SINo = j;
                            SQDetailViewModel.QuestionId = SR.QuestionId;
                            SQDetailViewModel.QuestionName = SR.SelfRiskAssessmentQuestion.Question;
                            SQDetailViewModel.Response = SR.Response;
                            SQDetailViewModel.Comments = SR.Comments;
                            SQTview.QuestionViewList.Add(SQDetailViewModel);
                            j = j + 1;
                        }

                    }
                    else
                    {
                        SQTview.AssessorName = "";
                        SQTview.AssessedDate = DateTime.Now;
                        SQTview.RiskType = FunctionType;
                        SQTview.Id = 0;
                        List<SelfRiskAssessmentQuestion> SRquestions = db.SelfRiskAssessmentQuestions.Where(x => x.RiskType == FunctionType).ToList();
                        int j = 1;
                        foreach (SelfRiskAssessmentQuestion SR in SRquestions)
                        {
                            SelfAssessmentQuestionTestDetailsViewModel SQDetailViewModel = new SelfAssessmentQuestionTestDetailsViewModel();
                            SQDetailViewModel.SINo = j;
                            SQDetailViewModel.QuestionId = SR.Id;
                            SQDetailViewModel.QuestionName = SR.Question;
                            SQDetailViewModel.Response = "";
                            SQDetailViewModel.Comments = "";
                            SQTview.QuestionViewList.Add(SQDetailViewModel);
                            j = j + 1;
                        }
                    }
                }

            }

            return SQTview;
        }

        public List<SelfAssessmentQuestionTestDetailsViewModelReport> ConvertToQuestionTestViewReport(string FunctionType, string FromDate)
        {
            DateTime Fromdt = DateTime.ParseExact(FromDate, "MM/dd/yyyy", CultureInfo.CurrentCulture);
            //DateTime Todt = DateTime.ParseExact(ToDate, "MM/dd/yyyy", CultureInfo.CurrentCulture);

            List<SelfAssessmentQuestionTestDetailsViewModelReport> SQTview = new List<SelfAssessmentQuestionTestDetailsViewModelReport>();
            using (var db = new RMSHIPAAEntities())
            {
                //var count = db.SelfRiskAssessmentTests.Where(x => x.AssessedDate >= Fromdt && x.AssessedDate <= Todt && x.RiskType == FunctionType).Count();
                var count = db.SelfRiskAssessmentTests.Where(x => x.AssessedDate >= Fromdt && x.AssessedDate <= Fromdt && x.RiskType == FunctionType).Count();
                if (count > 0)
                {
                    
                    List<SelfRiskAssessmentTest> SelfRiskTest = db.SelfRiskAssessmentTests.Where(x => x.AssessedDate >= Fromdt && x.AssessedDate <= Fromdt && x.RiskType == FunctionType).ToList();
                    foreach (SelfRiskAssessmentTest SRT in SelfRiskTest)
                    {                     
                        List<SelfRiskAssessmentTestDetail> SRTDetails = db.SelfRiskAssessmentTestDetails.Where(x => x.TestRefId == SRT.Id).ToList();
                        int j = 1;
                        foreach (SelfRiskAssessmentTestDetail SR in SRTDetails)
                        {
                            SelfAssessmentQuestionTestDetailsViewModelReport SQTVR = new SelfAssessmentQuestionTestDetailsViewModelReport();

                            SQTVR.AssessorName = SRT.AssessorName;
                            SQTVR.AssessedDate = SRT.AssessedDate;                           
                            SQTVR.SINo = j;                            
                            SQTVR.Questions = SR.SelfRiskAssessmentQuestion.Question;                            
                            if (SR.Response == "Yes")
                            {
                                SQTVR.ResponseStatus = "Yes";
                            }
                            else if (SR.Response == "No")
                            {
                                SQTVR.ResponseStatus = "No";
                            }
                            else 
                            {
                                SQTVR.ResponseStatus = "NA";
                            }
                            SQTVR.Comments = SR.Comments;
                            SQTview.Add(SQTVR);
                            j = j + 1;
                        }
                    }
                }
            }

            return SQTview;
        }

        //public DAL.SelfRiskAssessmentTest ConvertToSelfRiskAssessmentTest()
        //{
        //    var selfTest = new DAL.SelfRiskAssessmentTest
        //    {
        //        Id = Id,
        //        AssessorName = AssessorName,
        //        AssessedDate = AssessedDate,
        //        CreatedBy = "Indidge",
        //        CreatedOn = DateTime.Now
        //    };
        //    return selfTest;
        //}
    }
}