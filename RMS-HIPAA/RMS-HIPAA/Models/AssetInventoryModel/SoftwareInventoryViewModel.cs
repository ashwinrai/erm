﻿using RMS_HIPAA.DAL;
using RMS_HIPAA.Framework.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RMS_HIPAA.Models.AssetInventoryModel
{
    public class SoftwareInventoryViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Asset Id")]
        public string AssetId { get; set; }

        [Display(Name ="Asset Class")]
        public string AssetClass { get; set; }
        public SelectList AssetClassList { get; private set; }

        [Display(Name ="Name of Application")]
        public string ApplicationName { get; set; }

        [Display(Name ="Description")]
        public string Description { get; set; }

        [Display(Name ="Operating System")]
        public string Os { get; set; }
        public SelectList OSList { get; private set; }

        [Display(Name ="Shared")]
        public string Shared { get; set; }

        [Display(Name ="Application Criticality")]
        public string ApplicationCriticality { get; set; }
        public SelectList ApplicationCriticalityList { get; private set; }

        [Display(Name ="Data Classification")]
        public string DataClassification { get; set; }
        public SelectList DataClassificationList { get; private set; }

        [Display(Name ="Risk assess frequency")]
        public string RiskAssessFreq { get; set; }
        public SelectList RiskAssessFreqList { get; private set; }

        [Display(Name ="Business Function")]
        public string BusinessFunction { get; set; }
        public SelectList BusinessFunctionList { get; private set; }

        [Display(Name ="System/Business Owner")]
        public string BusinessOwnerName { get; set; }

        [Display(Name ="Department/Function")]
        public string Department { get; set; }
        public SelectList DepartmentList { get; private set; }

        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }

        public SoftwareInventoryViewModel()
        {

        }
        public async Task InitializeValues()
        {
            AssetId = RiskAssessmentService.GenerateAssetID();
            await PopulateListData(null, null, null, null, null, null,null,null);
        }
        private async Task PopulateListData(string AssetClassVal,string OSVal,string CategoryVal,string ApplicationCriticalityVal,string DataClassificationVal,string AssessFreqVal,string FunctionVal,string DeptVal)
        {
            try
            {
                using (var db = new RMSHIPAAEntities())
                {
                    var responsibleDepts = await db.Departments.ToListAsync();
                    DepartmentList = new SelectList(responsibleDepts, "Name", "Name", DeptVal);

                    var assetClasses = new List<KeyValuePair<string, string>>
                    {
                        //ePHI Software, Operating System, Server Software, Oracle Database, SQL Database
                        new KeyValuePair<string, string>("ePHI Software", "ePHI Software"),
                        new KeyValuePair<string, string>("Operating System", "Operating System"),
                        new KeyValuePair<string, string>("Server Software", "Server Software"),
                        new KeyValuePair<string, string>("Oracle Database", "Oracle Database"),
                        new KeyValuePair<string, string>("SQL Database", "SQL Database"),
                    };
                    AssetClassList= new SelectList(assetClasses, "Key", "Value", AssetClassVal);

                    var os= new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("Windows", "Windows"),                        
                        new KeyValuePair<string, string>("Linux", "Linux"),
                        new KeyValuePair<string, string>("MAC", "MAC"),

                    };
                    OSList = new SelectList(os, "Key", "Value", OSVal);

                    var applicationCriticality = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("High", "High"),
                        new KeyValuePair<string, string>("Medium", "Medium"),
                        new KeyValuePair<string, string>("Low", "Low"),
                    };
                    ApplicationCriticalityList = new SelectList(applicationCriticality, "Key", "Value", ApplicationCriticalityVal);

                   var dataClassification= new List<KeyValuePair<string, string>>
                    {
                       //Confidential [Contains - ePHI], Sensitive [Non-PHI, but business ctitial]
                        new KeyValuePair<string, string>("Confidential [Contains - ePHI]", "Confidential [Contains - ePHI]"),
                        new KeyValuePair<string, string>("Sensitive [Non-PHI, but business ctitial]", "Sensitive [Non-PHI, but business ctitial]"),
                        
                    };
                    DataClassificationList = new SelectList(dataClassification, "Key", "Value", DataClassificationVal);

                    var riskAssessFreq= new List<KeyValuePair<string, string>>
                    {
                        //As per policy, Ahdoc/Need basis.

                        new KeyValuePair<string, string>("As per policy", "As per policy"),
                        new KeyValuePair<string, string>("Ahdoc/Need basis", "Ahdoc/Need basis"),                       
                    };
                    RiskAssessFreqList= new SelectList(riskAssessFreq, "Key", "Value", AssessFreqVal);

                    var business= new List<KeyValuePair<string, string>>
                    {
                        //Patient Treatment, Patient billing, Healthcare Operations, Communications
                        new KeyValuePair<string, string>("Patient Treatment", "Patient Treatment"),
                        new KeyValuePair<string, string>("Patient billing", "Patient billing"),
                        new KeyValuePair<string, string>("Healthcare Operations", "Healthcare Operations"),
                        new KeyValuePair<string, string>("Communications", "Communications"),
                    };
                    BusinessFunctionList = new SelectList(business, "Key", "Value", FunctionVal);
                }
                    
            }
            catch (Exception ex)
            {
                RMS_HIPAA.Framework.Log.Logger.LogError(ex);
            }
        }

        public DAL.SoftwareInventory ConvertToSoftwareInventory()
        {
            var SI = new DAL.SoftwareInventory
            {
                AssetId = AssetId,
                AssetClass=AssetClass,
                ApplicationName=ApplicationName,
                Description=Description,
                OperatingSystem=Os,
                Shared=Shared,
                ApplicationCriticality=ApplicationCriticality,
                DataClassification=DataClassification,
                RiskAssessFrequencey=RiskAssessFreq,
                BusinessFunction=BusinessFunction,
                BusinessOwnerName=BusinessOwnerName,
                Department=Department,
                CreatedBy="Indidge",
                CreatedOn=DateTime.UtcNow
            };
            return SI;
        }

        public SoftwareInventoryViewModel ConvertFromSoftwareInventory(DAL.SoftwareInventory SI)
        {
            Id = SI.Id;
            AssetId = SI.AssetId;
            AssetClass = SI.AssetClass;
            ApplicationName = SI.ApplicationName;
            Description = SI.Description;
            Os = SI.OperatingSystem;
            Shared = SI.Shared;
            ApplicationCriticality = SI.ApplicationCriticality;
            DataClassification = SI.DataClassification;
            RiskAssessFreq = SI.RiskAssessFrequencey;
            BusinessFunction = SI.BusinessFunction;
            BusinessOwnerName = SI.BusinessOwnerName;
            Department = SI.Department;
            CreatedBy = SI.CreatedBy;
            CreatedOn = SI.CreatedOn;
            ModifiedBy = SI.ModifiedBy;
            ModifiedOn = SI.ModifiedOn;

            return this;
        }

        public SoftwareInventoryViewModel(DAL.SoftwareInventory SI)
        {
            Id = SI.Id;
            AssetId = SI.AssetId;
            AssetClass = SI.AssetClass;
            ApplicationName = SI.ApplicationName;
            Description = SI.Description;
            Os = SI.OperatingSystem;
            Shared = SI.Shared;
            ApplicationCriticality = SI.ApplicationCriticality;
            DataClassification = SI.DataClassification;
            RiskAssessFreq = SI.RiskAssessFrequencey;
            BusinessFunction = SI.BusinessFunction;
            BusinessOwnerName = SI.BusinessOwnerName;
            Department = SI.Department;
            CreatedBy = SI.CreatedBy;
            CreatedOn = SI.CreatedOn;
            ModifiedBy = SI.ModifiedBy;
            ModifiedOn = SI.ModifiedOn;
        }
    }
}