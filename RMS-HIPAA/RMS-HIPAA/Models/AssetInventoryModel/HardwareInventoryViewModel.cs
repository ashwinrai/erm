﻿using RMS_HIPAA.DAL;
using RMS_HIPAA.Framework.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace RMS_HIPAA.Models.AssetInventoryModel
{
    public class HardwareInventoryViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Asset Id")]
        public string AssetId { get; set; }

        [Display(Name = "Asset Class")]
        public string AssetClass { get; set; }
        public SelectList AssetClassList { get; private set; }

        [Display(Name = "Host/System Name")]
        public string SystemName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Make/Model")]
        public string Model { get; set; }
       

        [Display(Name = "Type")]
        public string Type { get; set; }

        [Display(Name = "Hardware Criticality")]
        public string HardwareCriticality { get; set; }
        public SelectList HardwareCriticalityList { get; private set; }

        [Display(Name = "Risk assess frequency")]
        public string RiskAssessFreq { get; set; }
        public SelectList RiskAssessFreqList { get; private set; }

        [Display(Name = "Business Function")]
        public string BusinessFunction { get; set; }
        public SelectList BusinessFunctionList { get; private set; }

        [Display(Name = "System/Business Owner")]
        public string BusinessOwnerName { get; set; }

        [Display(Name = "Department/Function")]
        public string Department { get; set; }
        public SelectList DepartmentList { get; private set; }

        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
        public HardwareInventoryViewModel() { }

        public async Task InitializeValues()
        {
            AssetId = RiskAssessmentService.GenerateAssetID();
            await PopulateListData(null,  null, null, null, null);
        }
        private async Task PopulateListData(string AssetClassVal,string HardwareCriticalityVal,  string AssessFreqVal, string FunctionVal, string DeptVal)
        {
            try
            {
                using (var db = new RMSHIPAAEntities())
                {
                    var responsibleDepts = await db.Departments.ToListAsync();
                    DepartmentList = new SelectList(responsibleDepts, "Name", "Name", DeptVal);

                    var assetClasses = new List<KeyValuePair<string, string>>
                    {
                        //Laptop, Mobile/Handheld devices,Network Infrastructure, Server Hadware,
                        new KeyValuePair<string, string>("Laptop", "Laptop"),
                        new KeyValuePair<string, string>("Mobile/Handheld devices", "Mobile/Handheld devices"),
                        new KeyValuePair<string, string>("Network Infrastructure", "Network Infrastructure"),
                        new KeyValuePair<string, string>("Server Hadware", "Server Hadware"),
                    };
                    AssetClassList = new SelectList(assetClasses, "Key", "Value", AssetClassVal);

                    

                    var hardwareCriticality = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("High", "High"),
                        new KeyValuePair<string, string>("Medium", "Medium"),
                        new KeyValuePair<string, string>("Low", "Low"),
                    };
                    HardwareCriticalityList = new SelectList(hardwareCriticality, "Key", "Value", HardwareCriticalityVal);

                    

                    var riskAssessFreq = new List<KeyValuePair<string, string>>
                    {
                        //As per policy, Ahdoc/Need basis.

                        new KeyValuePair<string, string>("As per policy", "As per policy"),
                        new KeyValuePair<string, string>("Ahdoc/Need basis", "Ahdoc/Need basis"),
                    };
                    RiskAssessFreqList = new SelectList(riskAssessFreq, "Key", "Value", AssessFreqVal);

                    var business = new List<KeyValuePair<string, string>>
                    {
                        //Patient Treatment, Patient billing, Healthcare Operations, Communications

                        new KeyValuePair<string, string>("Patient Treatment", "Patient Treatment"),
                        new KeyValuePair<string, string>("Patient billing", "Patient billing"),
                        new KeyValuePair<string, string>("Healthcare OperationsOption 3", "Healthcare Operations"),
                        new KeyValuePair<string, string>("Communications", "Communications"),
                    };
                    BusinessFunctionList = new SelectList(business, "Key", "Value", FunctionVal);
                }

            }
            catch (Exception ex)
            {
                RMS_HIPAA.Framework.Log.Logger.LogError(ex);
            }
        }

        public DAL.HardwareInventory ConvertToHardwareInventory()
        {
            var SI = new DAL.HardwareInventory
            {
                AssetId = AssetId,
                AssetClass = AssetClass,
                SystemName = SystemName,
                Description = Description,
                Model = Model,
               // Type = Type,
                HardwareCriticality = HardwareCriticality,                
                RiskAssessFrequencey = RiskAssessFreq,
                BusinessFunction = BusinessFunction,
                BusinessOwner = BusinessOwnerName,
                Department = Department,
                CreatedBy = "Indidge",
                CreatedOn = DateTime.UtcNow
            };
            return SI;
        }

        public HardwareInventoryViewModel ConvertFromSoftwareInventory(DAL.HardwareInventory SI)
        {
            Id = SI.Id;
            AssetId = SI.AssetId;
            AssetClass = SI.AssetClass;
            SystemName = SI.SystemName;
            Description = SI.Description;
            Model = SI.Model;
            Type=SI.Type;
            HardwareCriticality = SI.HardwareCriticality;
            RiskAssessFreq = SI.RiskAssessFrequencey;
            BusinessFunction = SI.BusinessFunction;
            BusinessOwnerName = SI.BusinessOwner;
            Department = SI.Department;
            CreatedBy = SI.CreatedBy;
            CreatedOn = SI.CreatedOn;
            ModifiedBy = SI.ModifiedBy;
            ModifiedOn = SI.ModifiedOn;

            return this;
        }

        public HardwareInventoryViewModel(DAL.HardwareInventory SI)
        {
            Id = SI.Id;
            AssetId = SI.AssetId;
            AssetClass = SI.AssetClass;
            SystemName = SI.SystemName;
            Description = SI.Description;
            Model = SI.Model;
            Type=SI.Type;
            HardwareCriticality = SI.HardwareCriticality;
            RiskAssessFreq = SI.RiskAssessFrequencey;
            BusinessFunction = SI.BusinessFunction;
            BusinessOwnerName = SI.BusinessOwner;
            Department = SI.Department;
            CreatedBy = SI.CreatedBy;
            CreatedOn = SI.CreatedOn;
            ModifiedBy = SI.ModifiedBy;
            ModifiedOn = SI.ModifiedOn;
        }
    }
}