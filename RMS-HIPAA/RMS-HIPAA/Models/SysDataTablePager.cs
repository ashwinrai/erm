﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Models
{
    public class SysDataTablePager
    {
        public string sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public List<string[]> aaData { get; set; }
    }
}