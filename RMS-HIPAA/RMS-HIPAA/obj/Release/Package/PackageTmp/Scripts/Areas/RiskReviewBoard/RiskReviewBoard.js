﻿
function LoadTables() {   


    var pTable = $('#PhysicalRisksList').DataTable({
        "dom": '<"toolbarP">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/RiskReviewBoardDetails",        
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": "Physical" })            
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 10,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
             {
                 "sName": "cbSel",
                 "bSearchable": false,
                 "bSortable": false,
                 "mRender": function (data, type, oObj) {
                     return '<div class="center"><label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label></div>'
                 }
             },
             {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },
            {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskAgent",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },      
               {
                  "sName": "RiskDescription",
                  "sWidth": "20%",
                  "bSearchable": true,
                  "bSortable": false
               },
               {
                   "sName": " ThreatVulnerabilityDescription",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "RiskImpactCategory",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ActionProposed",
                    "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                  "sName": "TargetDate",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
               },
               {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {
                     
                      return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="editRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Physical\')"><i class="ace-icon fa fa-pencil"></i></a><a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Physical\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a><a class="green" href="#" onClick="approveRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Physical\')"><i class="ace-icon fa fa-check-square-o bigger-130"></i></a></div>'
                  }
       
              }
        ],
        select: {
            style: 'multi'
        }
    });
    $("div.toolbarP").html('<p><button class="btn btn-white btn-success btn-sm" id="approvePBtn"><i class="ace-icon fa fa-check bigger-sm"></i>Approve selected</button>&nbsp;<button class="btn btn-white btn-danger btn-sm" id="deletePBtn"><i class="ace-icon fa fa-trash-o  red"></i>Delete selected</button></p>');

    var aTable = $('#AdministrativeRisksList').DataTable({
        "dom": '<"toolbarA">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/RiskReviewBoardDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": "Administrative" })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 10,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
            {
                "sName": "cbSel",
                "bSearchable": false,
                "bSortable": false,
                "mRender": function (data, type, oObj) {
                    return '<div class="center"><label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label></div>'
                }
            },
             {
                 "sName": "ID",
                 "bSearchable": false,
                 "bSortable": false,
                 "bVisible": false
             },
              {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskAgent",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "RiskDescription",
                   "sWidth": "20%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": " ThreatVulnerabilityDescription",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "RiskImpactCategory",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ActionProposed",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "TargetDate",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "Actions",
                   "bSearchable": false,
                   "bSortable": false,
                   "sWidth": "10%",
                   "mRender": function (data, type, oObj) {
                       return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="editRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Administrative\')"><i class="ace-icon fa fa-pencil"></i></a><a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Administrative\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a><a class="green" href="#" onClick="approveRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Administrative\')"><i class="ace-icon fa fa-check-square-o bigger-130"></i></a></div>'
                   }

               }
        ],
        select: {
            style: 'multi'
        }
    });
    $("div.toolbarA").html('<p><button class="btn btn-white btn-success btn-sm" id="approveABtn"><i class="ace-icon fa fa-check bigger-sm"></i>Approve selected</button>&nbsp;<button class="btn btn-white btn-danger btn-sm" id="deleteABtn"><i class="ace-icon fa fa-trash-o  red"></i>Delete selected</button></p>');

    var tTable = $('#TechnologyRisksList').DataTable({
         "dom": '<"toolbarT">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/RiskReviewBoardDetails",        
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": "Technology" })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 10,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
            {
                "sName": "cbSel",
                "bSearchable": false,
                "bSortable": false,
                "mRender": function (data, type, oObj) {
                    return '<div class="center"><label class="pos-rel"><input type="checkbox" class="ace" /><span class="lbl"></span></label></div>'
                }
            },
             {
                 "sName": "ID",
                 "bSearchable": false,
                 "bSortable": false,
                 "bVisible": false
             },
              {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskAgent",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },      
               {
                  "sName": "RiskDescription",
                  "sWidth": "20%",
                  "bSearchable": true,
                  "bSortable": false
               },
               {
                   "sName": " ThreatVulnerabilityDescription",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "RiskImpactCategory",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ActionProposed",
                    "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                  "sName": "TargetDate",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
               },
               {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {
                      return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="editRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Technology\')"><i class="ace-icon fa fa-pencil"></i></a><a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Technology\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a><a class="green" href="#" onClick="approveRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Technology\')"><i class="ace-icon fa fa-check-square-o bigger-130"></i></a></div>'
                  }
       
              }
        ],
        select: {
            style: 'multi'
        }
    });

    $("div.toolbarT").html('<p><button class="btn btn-white btn-success btn-sm" id="approveTBtn"><i class="ace-icon fa fa-check bigger-sm"></i>Approve selected</button>&nbsp;<button class="btn btn-white btn-danger btn-sm" id="deleteTBtn"><i class="ace-icon fa fa-trash-o  red"></i>Delete selected</button></p>');

    //approved Risks table
    var approvedRiskTable = $('#ApprovedRisksList').DataTable({
        //"dom": '<"toolbar">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/RiskReviewBoardDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": "Approved" })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 10,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
            
             {
                 "sName": "ID",
                 "bSearchable": false,
                 "bSortable": false,
                 "bVisible": false
             },
              {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Department",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "RiskDescription",
                   "sWidth": "20%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": " ThreatVulnerabilityDescription",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "RiskImpactCategory",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ActionProposed",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "TargetDate",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ApprovedOn",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "Actions",
                   "bSearchable": false,
                   "bSortable": false,
                   "sWidth": "10%",
                   "mRender": function (data, type, oObj) {
                       return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="viewRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Technology\')"><i class="ace-icon fa fa-eye"></i></a></div>'
                   }

               }
        ],
        select: {
            style: 'multi'
        }
    });
    
    /////////////////////////////////
    //table checkboxes
    $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

    //select/deselect all rows according to table header checkbox
    $('#AdministrativeRisksList > thead > tr > th input[type=checkbox], #AdministrativeRisksList_wrapper input[type=checkbox]').eq(0).on('click', function (event) {
        var th_checked = this.checked;//checkbox inside "TH" table header

        $('#AdministrativeRisksList').find('tbody > tr').each(function (e) {
            //alert("1");
            var row = this;
            if (th_checked) aTable.row(row).select();
            else aTable.row(row).deselect();
        });
    });

    //select/deselect a row when the checkbox is checked/unchecked
    $('#AdministrativeRisksList').on('click', 'td input[type=checkbox]', function (event) {
        var row = $(this).closest('tr').get(0);
        if (!this.checked) aTable.row(row).deselect();
        else aTable.row(row).select();
    });

    aTable.on('select', function (e, dt, type, index) {
        if (type === 'row') {
            $(aTable.row(index).node()).find('input:checkbox').prop('checked', true);
        }
    });
    aTable.on('deselect', function (e, dt, type, index) {
        if (type === 'row') {
            $(aTable.row(index).node()).find('input:checkbox').prop('checked', false);
        }
    });

    /////////////////////////////////
    //table checkboxes
   // $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

    //select/deselect all rows according to table header checkbox
    $('#PhysicalRisksList > thead > tr > th input[type=checkbox], #PhysicalRisksList_wrapper input[type=checkbox]').eq(0).on('click', function (event) {
        var th_checked = this.checked;//checkbox inside "TH" table header

        $('#PhysicalRisksList').find('tbody > tr').each(function (e) {
            //alert("1");
            var row = this;
            if (th_checked) pTable.row(row).select();
            else pTable.row(row).deselect();
        });
    });

    //select/deselect a row when the checkbox is checked/unchecked
    $('#PhysicalRisksList').on('click', 'td input[type=checkbox]', function (event) {
        var row = $(this).closest('tr').get(0);
        if (!this.checked) pTable.row(row).deselect();
        else pTable.row(row).select();
    });

    pTable.on('select', function (e, dt, type, index) {
        if (type === 'row') {
            $(pTable.row(index).node()).find('input:checkbox').prop('checked', true);
        }
    });
    pTable.on('deselect', function (e, dt, type, index) {
        if (type === 'row') {
            $(pTable.row(index).node()).find('input:checkbox').prop('checked', false);
        }
    });


    /////////////////////////////////
    //table checkboxes
   // $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

    //select/deselect all rows according to table header checkbox
    $('#TechnologyRisksList > thead > tr > th input[type=checkbox], #TechnologyRisksList_wrapper input[type=checkbox]').eq(0).on('click', function (event) {
        var th_checked = this.checked;//checkbox inside "TH" table header

        $('#TechnologyRisksList').find('tbody > tr').each(function (e) {
            //alert("1");
            var row = this;
            if (th_checked) tTable.row(row).select();
            else tTable.row(row).deselect();
        });
    });

    //select/deselect a row when the checkbox is checked/unchecked
    $('#TechnologyRisksList').on('click', 'td input[type=checkbox]', function (event) {
        var row = $(this).closest('tr').get(0);
        if (!this.checked) tTable.row(row).deselect();
        else tTable.row(row).select();
    });

    tTable.on('select', function (e, dt, type, index) {
        if (type === 'row') {
            $(tTable.row(index).node()).find('input:checkbox').prop('checked', true);
        }
    });
    tTable.on('deselect', function (e, dt, type, index) {
        if (type === 'row') {
            $(tTable.row(index).node()).find('input:checkbox').prop('checked', false);
        }
    });


    ///multiple delete
    $("#deletePBtn").on('click', function () {
       
        var deletedIds = new Array();
        var ids = $.map(pTable.rows('.selected').data(), function (item) {
            deletedIds.push(item[0]);
            return item[0]
        });
       
        deleteMultipleRiskAssessment(deletedIds);
    })
    $("#deleteABtn").on('click', function () {

        var deletedIds = new Array();
        var ids = $.map(aTable.rows('.selected').data(), function (item) {
            deletedIds.push(item[0]);
            return item[0]
        });
        deleteMultipleRiskAssessment(deletedIds);
    })
    $("#deleteTBtn").on('click', function () {

        var deletedIds = new Array();
        var ids = $.map(tTable.rows('.selected').data(), function (item) {
            deletedIds.push(item[0]);
            return item[0]
        });
        deleteMultipleRiskAssessment(deletedIds);
    })

    $("#approvePBtn").on('click', function () {
        var approvalIds = new Array();
        var ids = $.map(pTable.rows('.selected').data(), function (item) {
            approvalIds.push(item[0]);
            return item[0]
        });
        approveMultipleRiskAssessment(approvalIds)
    })

    
    $("#approveABtn").on('click', function () {
        var approvalIds = new Array();
        var ids = $.map(aTable.rows('.selected').data(), function (item) {
            approvalIds.push(item[0]);
            return item[0]
        });
        approveMultipleRiskAssessment(approvalIds)
    })
    $("#approveTBtn").on('click', function () {
        var approvalIds = new Array();
        var ids = $.map(tTable.rows('.selected').data(), function (item) {
            approvalIds.push(item[0]);
            return item[0]
        });
        approveMultipleRiskAssessment(approvalIds)
    })

}

function approveMultipleRiskAssessment(approvalIds)
{
    if (approvalIds.length <= 0) {
        var divMessage = '<div class="form-group"><p>' + 'You have not selected any risk! Please select atleast one before Approve</p></div>';
        var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
        $(divMessage).dialog({
            modal: true,
            autoOpen: true,
            show: {
                effect: "blind",
                duration: 500
            },
            buttons: [{
                text: "OK",
                "class": "btn btn-primary btn-xs",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    $(this).dialog("close");
                }
            }]
        }).siblings('.ui-dialog-titlebar').html(dlgtitle);
    }
    else {
        var postData = { "selectedIds": approvalIds };

        var divMessage = '<div class="form-group"><p>' + 'Would you like to approve selected Risks?</p></div>';
        var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';

        $(divMessage).dialog({
            modal: true,
            autoOpen: true,
            show: {
                effect: "blind",
                duration: 500
            },
            //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
            //title_html: true,
            buttons: [{
                text: "YES",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    var CurrentDialog = $(this);
                    $.ajax({
                        type: "put",
                        url: "../api/RiskReviewBoardDetails",
                        data: postData,
                        dataType: "json",       //    if you want to return json data, enable this data type.
                        success: function (data) {
                            CurrentDialog.dialog("close");
                            LoadTables();
                            HIPAA.Alert("All selected risks approved successfully", "success");

                        },
                        error: function (request, status, error) {
                            CurrentDialog.dialog("close");
                            HIPAA.Alert("Failed to approve selected risks", "danger");
                        }
                    });
                }
            }, {
                text: "NO",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    $(this).dialog("close");
                }
            }]
        }).siblings('.ui-dialog-titlebar').html(dlgtitle);

    }
}

function deleteMultipleRiskAssessment(selectedIds)
{
    if (selectedIds.length <= 0) {
        var divMessage = '<div class="form-group"><p>' + 'You have not selected any risk! Please select atleast one before Delete</p></div>';
        var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
        $(divMessage).dialog({
            modal: true,
            autoOpen: true,
            show: {
                effect: "blind",
                duration: 500
            },           
            buttons: [{
                text: "OK",
                "class": "btn btn-primary btn-xs",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    $(this).dialog("close");
                }
            }]
        }).siblings('.ui-dialog-titlebar').html(dlgtitle);
    }
    else {
        var postData = { "selectedIds": selectedIds };

        var divMessage = '<div class="form-group"><p>' + 'Would you like to delete slected Risks?</p></div>';
        var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';


       
        $(divMessage).dialog({
            modal: true,
            autoOpen: true,
            show: {
                effect: "blind",
                duration: 500
            },
            //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
            //title_html: true,
            buttons: [{
                text: "YES",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    var CurrentDialog = $(this);
                    $.ajax({
                        type: "delete",
                        url: "../api/RiskReviewBoardDetails",
                        data: postData,
                        dataType: "json",       //    if you want to return json data, enable this data type.
                        success: function (data) {
                            CurrentDialog.dialog("close");
                            LoadTables();
                            HIPAA.Alert("All selected risks deleted successfully", "success");

                        },
                        error: function (request, status, error) {
                            CurrentDialog.dialog("close");
                            HIPAA.Alert("Failed to delete the selected risks", "danger");
                        }
                    });
                }
            }, {
                text: "NO",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    $(this).dialog("close");
                }
            }]
        }).siblings('.ui-dialog-titlebar').html(dlgtitle);

    }
    
}

function editRiskAssessment(Id, RiskId, FunctionName) {
    if (FunctionName == "Physical")
    {
        var url = "../RiskReviewBoard/_editPhysicalRisks?id=" + Id;
        $("#editRisk").dialog({
            position: { my: "center", at: "top+100" },
            autoOpen: true,
            width: 1000,
            resizable: false,
            title: 'Risk Details',
            title_html: true,
            modal: true,
            open: function () {
                $(this).load(url);
                $('#editRisk').css('overflow', 'hidden');
            }
        });
    }
    else if(FunctionName=="Administrative")
    {
        var url = "../RiskReviewBoard/_editAdministrativeRisk?id=" + Id;
        $("#editRisk").dialog({
            position: { my: "center", at: "top+100" },
            autoOpen: true,
            width: 1000,
            resizable: false,
            title: 'Risk Details',
            title_html: true,
            modal: true,
            open: function () {
                $(this).load(url);
                $('#editRisk').css('overflow', 'hidden');
            }
        });
    }
    else {
        var url = "../RiskReviewBoard/_editTechnologyRisk?id=" + Id;
        $("#editRisk").dialog({
            position: { my: "center", at: "top+100" },
            autoOpen: true,
            width: 1000,
            resizable: false,
            title: 'Risk Details',
            title_html: true,
            modal: true,
            open: function () {
                $(this).load(url);
                $('#editRisk').css('overflow', 'hidden');
            }
        });
    }
}

function viewRiskAssessment(Id, RiskId, FunctionName) {
    var url = "../RiskReviewBoard/_viewRisk?id=" + Id;
    $("#viewRisk").dialog({
        position: { my: "center", at: "top+100" },
        autoOpen: true,
        width: 1000,
        resizable: false,
        title: 'Risk Details',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('#viewRisk').css('overflow', 'hidden');
        }
    });
}

function approveRiskAssessment(Id, RiskId, FunctionName) {
    var divMessage = '<div class="form-group"><p>' + 'Would you like to approve <b>' + RiskId + '</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
     $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "put",
                    url: "../api/RiskReviewBoardDetails?id=" + Id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        LoadTables();
                        HIPAA.Alert("Approved successfully", "success");

                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("Failed to approve the risk", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);
}

function deleteRiskAssessment(Id, RiskId, FunctionName) {
   
   
    var divMessage = '<div class="form-group"><p>' + 'Would you like to delete <b>' + RiskId + '</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
        //title_html: true,
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "delete",
                    url: "../api/RiskReviewBoardDetails?id=" + Id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        LoadTables();
                        HIPAA.Alert("RiskAssessment Details deleted successfully", "success");

                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("Failed to delete the RiskAssessment details", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);
}




