﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using RMS_HIPAA.Repository;
using System.Data.Entity.Validation;
using RMS_HIPAA.Framework.Service;
using RMS_HIPAA.Models;
using System.Collections.Specialized;

namespace RMS_HIPAA.Controllers
{
    [Route("api/[SelfRiskAssessmentTestDetails]")]
    public class SelfRiskAssessmentTestDetailsController : ApiController
    {
        public ISelfRiskAssessmentTestRepository<SelfAssessmentQuestionTestViewModel, int> _repository = null;

        //The Dependency Injection of the IRepository<TEnt, in TPk>
        public SelfRiskAssessmentTestDetailsController(ISelfRiskAssessmentTestRepository<SelfAssessmentQuestionTestViewModel, int> repo)
        {
            this._repository = repo;
        }

        public SelfRiskAssessmentTestDetailsController()
        {
            this._repository = new SelfRiskAssessmentTestRepository();
        }

        [HttpGet]
        public IHttpActionResult Get(string accesseddate, string FunctionName)
        {
            var model = _repository.Get(accesseddate, FunctionName);

            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpPost]
        public IHttpActionResult Post(SelfAssessmentQuestionTestViewModel selfRiskAssess)
        {
            try
            {
                _repository.Add(selfRiskAssess);
                // string RiskId = RiskAssessmentService.GenerateRiskId(RiskAssess.Risk);
                return Ok<string>("");
            }
            catch (DbEntityValidationException ex)
            {
                return InternalServerError(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }
    }
}
