﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RMS_HIPAA.Repository;
using RMS_HIPAA.Models;
using RMS_HIPAA.Models.AssetInventoryModel;
using System.Collections.Specialized;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Controllers
{
    [Route("api/[HardwareInventoryDetails]")]
    public class HardwareInventoryDetailsController : ApiController
    {
        public IHardwareInventoryRepository<HardwareInventoryViewModel, int, int> _repository = null;

        public HardwareInventoryDetailsController(IHardwareInventoryRepository<HardwareInventoryViewModel, int, int> repo)
        {
            this._repository = repo;
        }

        public HardwareInventoryDetailsController()
        {
            this._repository = new HardwareInventoryRepository();
        }

        [System.Web.Http.HttpGet]
        public SysDataTablePager Get()
        {
            var model = _repository.Get();

            IEnumerable<HardwareInventoryViewModel> filteredmodels;

            NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query);
            string sEcho = nvc["sEcho"].ToString();
            int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
            int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);
            string sSearch = nvc["sSearch"].ToString();
            if (!string.IsNullOrEmpty(sSearch))
            {
                //Used if particulare columns are filtered 
                var AssetIdFilter = Convert.ToString(nvc["sSearch_1"]);
                var AssetClassFilter = Convert.ToString(nvc["sSearch_2"]);
                var HostNameFilter = Convert.ToString(nvc["sSearch_3"]);
                var DescriptionFilter = Convert.ToString(nvc["sSearch_4"]);
                var ModelFilter = Convert.ToString(nvc["sSearch_5"]);
                var TypeFilter = Convert.ToString(nvc["sSearch_6"]);
                var HardwareCriticalityFilter = Convert.ToString(nvc["sSearch_7"]);               
                var RiskassessfrequenceyFilter = Convert.ToString(nvc["sSearch_8"]);
                var BusinessFunctionFilter = Convert.ToString(nvc["sSearch_9"]);
                var BusinessOwnerFilter = Convert.ToString(nvc["sSearch_10"]);
                var DepartmentFilter = Convert.ToString(nvc["sSearch_11"]);

                //Optionally check whether the columns are searchable at all 
                var isAssetIdSearchable = Convert.ToBoolean(nvc["bSearchable_1"]);
                var isAssetClassSearchable = Convert.ToBoolean(nvc["bSearchable_2"]);
                var isHostNameSearchable = Convert.ToBoolean(nvc["bSearchable_3"]);
                var isDescriptionSearchable = Convert.ToBoolean(nvc["bSearchable_4"]);
                var isModelSearchable = Convert.ToBoolean(nvc["bSearchable_5"]);
                var isTypeSearchable = Convert.ToBoolean(nvc["bSearchable_6"]);
                var isHardwareCriticalitySearchable = Convert.ToBoolean(nvc["bSearchable_7"]);               
                var isRiskassessfrequenceySearchable = Convert.ToBoolean(nvc["bSearchable_8"]);
                var isBusinessFunctionSearchable = Convert.ToBoolean(nvc["bSearchable_9"]);
                var isBusinessOwnerSearchable = Convert.ToBoolean(nvc["bSearchable_10"]);
                var isDepartmentSearchable = Convert.ToBoolean(nvc["bSearchable_11"]);

                filteredmodels = model
                     .Where(c => isAssetIdSearchable && c.AssetId.ToLower().Contains(sSearch.ToLower()) ||
                     isAssetClassSearchable && ((c.AssetClass != null) ? c.AssetClass.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isHostNameSearchable && ((c.SystemName != null) ? c.SystemName.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isDescriptionSearchable && ((c.Description != null) ? c.Description.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isModelSearchable && ((c.Model != null) ? c.Model.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isTypeSearchable && ((c.Type != null) ? c.Type.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isHardwareCriticalitySearchable && ((c.HardwareCriticality!= null) ? c.HardwareCriticality.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||                     
                     isRiskassessfrequenceySearchable && ((c.RiskAssessFreq != null) ? c.RiskAssessFreq.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isBusinessFunctionSearchable && ((c.BusinessFunction != null) ? c.BusinessFunction.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isBusinessOwnerSearchable && ((c.BusinessOwnerName != null) ? c.BusinessOwnerName.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isDepartmentSearchable && ((c.Department != null) ? c.Department.ToString().ToLower().Contains(sSearch.ToLower()) : false)
                     );
            }
            else
            {
                filteredmodels = model;
            }

            var isAssetIdSortable = Convert.ToBoolean(nvc["bSortable_1"]);
            var isAssetClassSortable = Convert.ToBoolean(nvc["bSortable_2"]);
            var isHostNameSortable = Convert.ToBoolean(nvc["bSortable_3"]);
            var isDescriptionSortable = Convert.ToBoolean(nvc["bSortable_4"]);
            var isModelSortable = Convert.ToBoolean(nvc["bSortable_5"]);
            var isTypeSortable = Convert.ToBoolean(nvc["bSortable_6"]);
            var isHardwareCriticalitySortable = Convert.ToBoolean(nvc["bSortable_7"]);            
            var isRiskassessfrequenceySortable = Convert.ToBoolean(nvc["bSortable_8"]);
            var isBusinessFunctionSortable = Convert.ToBoolean(nvc["bSortable_9"]);
            var isBusinessOwnerSortable = Convert.ToBoolean(nvc["bSortable_10"]);
            var isDepartmentSortable = Convert.ToBoolean(nvc["bSortable_11"]);

            var sortColumnIndex = Convert.ToInt32(nvc["iSortCol_0"]);
            Func<HardwareInventoryViewModel, string> orderingFunction = (c =>
                                                                    sortColumnIndex == 1 && isAssetIdSortable ? c.AssetId.ToString() :
                                                                    sortColumnIndex == 2 && isAssetClassSortable ? c.AssetClass :
                                                                    sortColumnIndex == 3 && isHostNameSortable? c.SystemName.ToString() :
                                                                    sortColumnIndex == 4 && isDescriptionSortable ? c.Description :
                                                                    sortColumnIndex == 5 && isModelSortable ? c.Model :
                                                                    sortColumnIndex == 6 && isTypeSortable ? c.Type :
                                                                    sortColumnIndex == 7 && isHardwareCriticalitySortable ? c.HardwareCriticality :
                                                                    sortColumnIndex == 8 && isRiskassessfrequenceySortable ? c.RiskAssessFreq :
                                                                    sortColumnIndex == 9 && isBusinessFunctionSortable ? c.BusinessFunction :
                                                                    sortColumnIndex == 10 && isBusinessOwnerSortable ? c.BusinessOwnerName :
                                                                    sortColumnIndex == 11 && isDepartmentSortable ? c.Department :
                                                                   "");


            var sortDirection = nvc["sSortDir_0"]; // asc or desc
            //if (sortDirection == "asc")
            //    filteredmodels = filteredmodels.OrderBy(orderingFunction);
            //else
            //    filteredmodels = filteredmodels.OrderByDescending(orderingFunction);

            var displayedtechnology = filteredmodels.Skip(iDisplayStart).Take(iDisplayLength);



            var CustomerPaged = new Models.SysDataTablePager();
            CustomerPaged.sEcho = sEcho;
            CustomerPaged.iTotalRecords = filteredmodels.Count();
            CustomerPaged.iTotalDisplayRecords = filteredmodels.Count();
            var result = from c in displayedtechnology select new[] { Convert.ToString(c.Id), Convert.ToString(c.AssetId), c.AssetClass, c.SystemName, c.Description, c.Model, c.Type, c.HardwareCriticality, c.RiskAssessFreq, c.BusinessFunction, c.BusinessOwnerName, c.Department, "actions" };
            CustomerPaged.aaData = result.ToList();

            return CustomerPaged;
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var model = _repository.Get(id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpPost]
        public IHttpActionResult Post(HardwareInventoryViewModel HWIDetails)
        {
            try
            {
                _repository.Add(HWIDetails);

                return Ok<string>("Success");
            }
            catch (DbEntityValidationException ex)
            {
                return InternalServerError(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]HardwareInventoryViewModel HWIDetails)
        {
            try
            {
                _repository.Update(HWIDetails);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
