﻿using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RMS_HIPAA.Controllers
{
    public class SelfRiskAssessmentTestController : Controller
    {
        // GET: AdministrativeSelfRiskAssessmentTest
        public ActionResult AdministrativeSelfRiskAssessmentTest()
        {
            SelfAssessmentQuestionTestViewModel SAT = new SelfAssessmentQuestionTestViewModel();
            SAT.AssessedDate = DateTime.Now;
            return View(SAT);
        }

        public ActionResult PhysicalSelfRiskAssessmentTest()
        {
            SelfAssessmentQuestionTestViewModel SAT = new SelfAssessmentQuestionTestViewModel();
            SAT.AssessedDate = DateTime.Now;
            return View(SAT);
        }

        public ActionResult TechnologySelfRiskAssessmentTest()
        {
            SelfAssessmentQuestionTestViewModel SAT = new SelfAssessmentQuestionTestViewModel();
            SAT.AssessedDate = DateTime.Now;
            return View(SAT);
        }
    }
}