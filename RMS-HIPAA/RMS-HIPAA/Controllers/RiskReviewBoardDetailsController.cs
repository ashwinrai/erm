﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Repository;
using System.Data.Entity.Validation;
using RMS_HIPAA.Framework.Service;
using RMS_HIPAA.Models;
using System.Collections.Specialized;

namespace RMS_HIPAA.Controllers
{
    public class SimpleViewModel
    {
        public Guid[] selectedIds { get; set; }
    }


    [Route("api/[RiskReviewBoardDetails]")]
    public class RiskReviewBoardDetailsController : ApiController
    {
        public IRiskReviewBoardRepository<RiskAssessmentViewModel, int, Guid> _repository = null;

        public RiskReviewBoardDetailsController(IRiskReviewBoardRepository<RiskAssessmentViewModel, int, Guid> repo)
        {
            this._repository = repo;
        }

        public RiskReviewBoardDetailsController()
        {
            this._repository = new RiskReviewBoardRepository();
        }

        [System.Web.Http.HttpGet]
        public SysDataTablePager Get(string FunctionName)
        {
            var model = _repository.Get(FunctionName);

          // model = model.Where(x => x.Approval == true).ToList();

            IEnumerable<RiskAssessmentViewModel> filteredmodels;

            NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query);
            string sEcho = nvc["sEcho"].ToString();
            int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
            int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);
            string sSearch = nvc["sSearch"].ToString();
            if (!string.IsNullOrEmpty(sSearch))
            {
                //Used if particulare columns are filtered  

                var RiskIdFilter = Convert.ToString(nvc["sSearch_1"]);
                var RiskAgentFilter = Convert.ToString(nvc["sSearch_2"]);
                var AssetNameFilter = Convert.ToString(nvc["sSearch_3"]);
                var AssessedDateFilter = Convert.ToString(nvc["sSearch_4"]);
                var RiskDescriptionFilter = Convert.ToString(nvc["sSearch_5"]);
               
                //Optionally check whether the columns are searchable at all 

                var isRiskIdSearchable = Convert.ToBoolean(nvc["bSearchable_1"]);
                var isRiskAgentSearchable = Convert.ToBoolean(nvc["bSearchable_2"]);
                var isAssetNameSearchable = Convert.ToBoolean(nvc["bSearchable_3"]);
                var isAssessedDateSearchable = Convert.ToBoolean(nvc["bSearchable_4"]);
                var isRiskDescriptionSearchable = Convert.ToBoolean(nvc["bSearchable_5"]);
                

                filteredmodels = model
               .Where(c =>
                           isRiskIdSearchable && c.RiskId.ToLower().Contains(sSearch.ToLower()) ||
                           isRiskAgentSearchable && ((c.RiskAgent != null) ? c.RiskAgent.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                           isAssetNameSearchable && ((c.AssetName != null) ? c.AssetName.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                           isAssessedDateSearchable && ((c.AssessedDate != null) ? c.AssessedDate.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                           isRiskDescriptionSearchable && ((c.AssessedDate != null) ? c.AssessedDate.ToString().ToLower().Contains(sSearch.ToLower()) : false));

            }
            else
            {
                filteredmodels = model;
            }

            var isRiskIdSortable = Convert.ToBoolean(nvc["bSortable_1"]);
            var isRiskAgentSortable = Convert.ToBoolean(nvc["bSortable_2"]);
            var isAssetNameSortable = Convert.ToBoolean(nvc["bSortable_3"]);
            var isAssessedDateSortable = Convert.ToBoolean(nvc["bSortable_4"]);
            var isRiskDescriptionSortable = Convert.ToBoolean(nvc["bSortable_5"]);
            var isApprovedOnSortable= Convert.ToBoolean(nvc["bSortable_9"]);
            var sortColumnIndex = Convert.ToInt32(nvc["iSortCol_0"]);
            Func<RiskAssessmentViewModel, string> orderingFunction = (c =>
                                                                    sortColumnIndex == 1 && isRiskIdSortable ? c.RiskId :
                                                                    sortColumnIndex == 2 && isRiskAgentSortable ? c.RiskAgent.ToString() :
            
                                                                    sortColumnIndex == 3 && isAssetNameSortable ? c.AssetName :
                                                                    sortColumnIndex == 4 && isAssessedDateSortable ? c.AssessedDate.ToString() :
                                                                    sortColumnIndex == 5 && isRiskDescriptionSortable ? c.RiskDescription :
                                                                     sortColumnIndex == 9 && isApprovedOnSortable ? c.ApprovedOn.ToString() :
                                                                   "");

            var sortDirection = nvc["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredmodels = filteredmodels.OrderBy(orderingFunction);
            else
                filteredmodels = filteredmodels.OrderByDescending(orderingFunction);

            var displayedtechnology = filteredmodels.Skip(iDisplayStart).Take(iDisplayLength);

            
            
            var CustomerPaged = new Models.SysDataTablePager();
            CustomerPaged.sEcho = sEcho;
            CustomerPaged.iTotalRecords = filteredmodels.Count();
            CustomerPaged.iTotalDisplayRecords = filteredmodels.Count();
            if(FunctionName== "Approved")
            {
                var result = from c in displayedtechnology select new[] { Convert.ToString(c.RiskAssessmentId), c.RiskId, c.Risk, c.RiskDescription, c.ThreatVulnerabilityDescription, c.RiskImpactCategory, c.RiskExposureLevel, c.ActionProposed, (c.TargetDate != null) ? Convert.ToString(c.TargetDate.Value.ToShortDateString()) : "", (c.ApprovedOn != null) ? Convert.ToString(c.ApprovedOn.Value.ToShortDateString()) : "", "actions" };
                CustomerPaged.aaData = result.ToList();
            }
            else
            {
                var result = from c in displayedtechnology select new[] { Convert.ToString(c.RiskAssessmentId), "", c.RiskId, Convert.ToString(c.RiskAgent), c.RiskDescription, c.ThreatVulnerabilityDescription, c.RiskImpactCategory, c.RiskExposureLevel, c.ActionProposed, (c.TargetDate != null) ? Convert.ToString(c.TargetDate.Value.ToShortDateString()) : "", "actions" };
                CustomerPaged.aaData = result.ToList();
            }
            return CustomerPaged;

        }

        [HttpPut]
        public IHttpActionResult Approve(Guid id)
        {
            try
            {
                _repository.Approve(id);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpPut]
        public IHttpActionResult ApproveAll(SimpleViewModel selectedIds)
        {
            var records = selectedIds.selectedIds;
            try
            {
                foreach (var id in records)
                {
                    _repository.Approve(id);
                    
                }
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(Guid id)
        {
            try
            {
                _repository.Delete(id);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpDelete]
        public IHttpActionResult DeleteAll(SimpleViewModel selectedIds)
        {
            var records = selectedIds.selectedIds;
            try
            {
                foreach(var id in records)
                {
                   
                    _repository.Delete(id);
                }
               //
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
