﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.DAL;

namespace RMS_HIPAA.Controllers
{
    public class PostImplementationRevewController : Controller
    {
        private RMSHIPAAEntities db = new RMSHIPAAEntities();
        // GET: PostImplementationRevew
        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult PostImplementationReviewTechnologyListView()
        {
            return View();
        }
        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PostImplementationReviewPhysicalListView()
        {
            return View();
        }
        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult PostImplementationReviewListView()
        {
            return View();
        }

        public async Task<PartialViewResult> _performPostImplementationReview(Guid id)
        {
            var model = new RiskAssessmentViewModel();
            var rsk = db.RiskAssessments.Where(r => r.Id == id).SingleOrDefault();
            await model.InitializeValues(rsk.Risk);
            model.RiskAssessmentId = rsk.Id;
            model.RiskId = rsk.RiskId;
            model.RiskAgent = rsk.RiskAgent;
            model.DepartmentName = rsk.DepartmentorFunctionName;
            model.AssetName = rsk.AssetName;
            model.Owner = rsk.AssessorOwner;
            model.RiskInput = rsk.RiskInput;
            model.ThreatSource = rsk.ThreatSource;
            model.RiskDescription = rsk.RiskDescription;
            model.ThreatVulnerabilityDescription = rsk.ThreatVulnerabilityDescription;
            model.ExistingControls = rsk.ExistingControl;
            model.RiskImpactCategory = rsk.RiskImpactCategory;
            model.ProbabilityofOccurence = rsk.ProbabilityofOccurence;
            model.ImpactLevel = rsk.ImpactLevel;
            model.RiskExposureLevel = rsk.RiskExposureLevel;
            model.ActionProposed = rsk.NewProposedControl;
            model.Responsible = rsk.Responsible;
            //(c.TargetDate != null) ? Convert.ToString(c.TargetDate.Value.ToShortDateString()) : ""
            model.AssessedDateVal = (rsk.AssessedDate != null) ? rsk.AssessedDate.Value.ToShortDateString() : "";
            model.TargetDateVal = (rsk.TargetDateImplement != null) ? rsk.TargetDateImplement.Value.ToShortDateString() : "";
            model.TargetQurter = rsk.TargetQuarter;
            model.ResponsibleDepartment = rsk.Department;
            model.RiskResponse = rsk.RiskResponse;
            model.Status = rsk.Status;
            model.ClosedDateVal = (rsk.ClosedDate != null) ? rsk.ClosedDate.Value.ToShortDateString() : "";
            return PartialView(model);
        }
    }
}