﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;


using RMS_HIPAA.DAL;
using RMS_HIPAA.Models;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Framework.Log;


namespace RMS_HIPAA.Controllers
{
    public class RiskReviewBoardController : Controller
    {
        private RMSHIPAAEntities db = new RMSHIPAAEntities();

        // GET: RiskReviewBoard
        [Authorize(Roles = "Risk_Officers")]
        public ActionResult RiskReviewBoardView()
        {
            return View();
        }

        public async Task<PartialViewResult> _editPhysicalRisks(Guid id)
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Physical");
            return PartialView(model);
        }

        public async Task<PartialViewResult> _editAdministrativeRisk(Guid id)
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Administrative");
            return PartialView(model);
        }

        public async Task<PartialViewResult> _editTechnologyRisk(Guid id)
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Technology");
            return PartialView(model);
        }
        public async Task<PartialViewResult> _viewRisk(Guid id)
        {
            var model = new RiskAssessmentViewModel();
            var rsk = db.RiskAssessments.Where(r => r.Id == id).SingleOrDefault();
            await model.InitializeValues(rsk.Risk);
            model.RiskId = rsk.RiskId;
            model.RiskAgent = rsk.RiskAgent;
            model.DepartmentName = rsk.DepartmentorFunctionName;
            model.AssetName = rsk.AssetName;
            model.Owner = rsk.AssessorOwner;
            model.RiskInput = rsk.RiskInput;
            model.ThreatSource = rsk.ThreatSource;
            model.RiskDescription = rsk.RiskDescription;
            model.ThreatVulnerabilityDescription = rsk.ThreatVulnerabilityDescription;
            model.ExistingControls = rsk.ExistingControl;
            model.RiskImpactCategory = rsk.RiskImpactCategory;
            model.ProbabilityofOccurence = rsk.ProbabilityofOccurence;
            model.ImpactLevel = rsk.ImpactLevel;
            model.RiskExposureLevel = rsk.RiskExposureLevel;
            model.ActionProposed = rsk.NewProposedControl;
            model.Responsible = rsk.Responsible;
            //(c.TargetDate != null) ? Convert.ToString(c.TargetDate.Value.ToShortDateString()) : ""
            model.AssessedDateVal=(rsk.AssessedDate!=null)?rsk.AssessedDate.Value.ToShortDateString():"";
            model.TargetDateVal = (rsk.TargetDateImplement!=null)?rsk.TargetDateImplement.Value.ToShortDateString():"";
            model.TargetQurter = rsk.TargetQuarter;
            model.ResponsibleDepartment = rsk.Department;
            model.RiskResponse = rsk.RiskResponse;
            model.Status = rsk.Status;
            model.ClosedDateVal = (rsk.ClosedDate!=null)?rsk.ClosedDate.Value.ToShortDateString():"";
            return PartialView(model);
        }
        [Authorize(Roles = "Risk_Officers")]
        public ActionResult KPIAdministrativeRisk()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult KPIPhysicalRisk()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult KPITechnologyRisk()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult KPIRisksView()
        {
            return View();
        }

        public JsonResult RiskTreeMapChart(string risk)
        {
            var query = (from r in db.RiskAssessments where r.Approval == true && r.RiskExposureLevel != null && r.ImpactLevel != null && r.ProbabilityofOccurence != null select r).ToList();
            var chartData = new object[query.Count() + 5];
            chartData[0] = new object[]{
                "Risk",
                "Risk Exposure Level",                              
                "Probability of Occurence",               // "Risk Domain",
                "Risk Exposure"

            };
            chartData[1] = new object[]{
                "Risk",
                null,
                0,               // "Risk Domain",
                0

            };
            chartData[3] = new object[]{
                "Medium",
                "Risk",
                0,               // "Risk Domain",
                0

            };
            chartData[2] = new object[]{
                "Low",
                "Risk",
                0,               // "Risk Domain",
                0

            };
            chartData[4] = new object[]{
                "High",
                "Risk",
                0,               // "Risk Domain",
                0

            };
            
            
            var j = 4;
            foreach (var r in query)
            {
                j++;
                var Re = r.ImpactLevel1.ImpactValue * r.ProbabilityofOccurence1.ProbabilityValue;
                //chartData[j] = new object[] { r.RiskExposureLevel, r.ImpactLevel1.ImpactValue, r.ProbabilityofOccurence1.ProbabilityValue, Re };
                chartData[j] = new object[] { r.RiskId, r.RiskExposureLevel, Re, Re };
            }
            return Json(chartData, JsonRequestBehavior.AllowGet);
        }
        public JsonResult RiskBubbleChart(string risk)
        {
            var query = (from r in db.RiskAssessments where r.Approval == true && r.RiskExposureLevel!=null && r.ImpactLevel != null && r.ProbabilityofOccurence!=null select r).ToList();
            var chartData = new object[query.Count() + 1];
            chartData[0] = new object[]{
                "Risk",
                "Impact",
                "Probability of Occurence",               // "Risk Domain",
                "Risk Exposure"

            };
            var j = 0;
            foreach (var r in query)
            {
                j++;
                var Re = r.ImpactLevel1.ImpactValue * r.ProbabilityofOccurence1.ProbabilityValue;
                //chartData[j] = new object[] { r.RiskExposureLevel, r.ImpactLevel1.ImpactValue, r.ProbabilityofOccurence1.ProbabilityValue, Re };
                chartData[j] = new object[] { r.RiskId, Re, r.ProbabilityofOccurence1.ProbabilityValue, Re };
            }
            return Json(chartData, JsonRequestBehavior.AllowGet);
        }



        public JsonResult GetHighMediumARiskData(string risk)
        {
            //await Task.Delay(10);
            var query = from ara in db.RiskAssessments
                        group ara by ara.CreatedOn.Year into ara
                        select new
                        {
                            Year = ara.Key
                        };
            var chartData1 = new object[query.Count() + 1];
            chartData1[0] = new object[]{
                "Year", "Medium Risk", "High Risk"
            };
            int high, medium;
            var j = 0;
            foreach (var ara in query)
            {
                j++;
                high = (from h in db.RiskAssessments where h.CreatedOn.Year == ara.Year && h.RiskExposureLevel == "High" && h.Risk == risk && h.Approval == true && h.Status != null && h.Status != "Close" select h).Count();
                medium = (from m in db.RiskAssessments where m.CreatedOn.Year == ara.Year && m.RiskExposureLevel == "Medium" && m.Risk == risk && m.Approval == true && m.Status != null && m.Status != "Close" select m).Count();
                chartData1[j] = new object[] { ara.Year.ToString(), medium, high };
            }

            return Json(chartData1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetStatuswiseARiskData(string risk)
        {
            //await Task.Delay(10);
            var query = db.RiskAssessments.Where(r => r.Risk == risk && r.Approval == true && r.Status != null ).GroupBy(ara => ara.Status).Select(d => new
            {
                Stat = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData2 = new object[query.Count() + 1];
            chartData2[0] = new object[]{
                "Status",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Stat ?? "Unassigned";

                chartData2[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData2, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRELARiskData(string risk)
        {
            //await Task.Delay(10);
            var query = db.RiskAssessments.Where(r => r.Risk == risk && r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskExposureLevel!=null).GroupBy(ara => ara.RiskExposureLevel).Select(d => new
            {
                Stat = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData2 = new object[query.Count() + 1];
            chartData2[0] = new object[]{
                "Risk Exposure",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Stat ?? "Unassigned";

                chartData2[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData2, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetImpactCategoryARiskData(string risk)
        {
            //await Task.Delay(10);
            var query = db.RiskAssessments.Where(r => r.Risk == risk && r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
            {
                Category = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData3 = new object[query.Count() + 1];
            chartData3[0] = new object[]{
                "Impact Category",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Category ?? "Unassigned";

                chartData3[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData3, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetImpactCategoryRiskDataRRB(string risk)
        {

            var query = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
            {
                Category = d.Key,
                RiskAssessment = d.Count()
            });
            var total = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).Count();
            var chartData3 = new object[query.Count() + 1];
            chartData3[0] = new object[]{
                "Impact Category",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Category ?? "Unassigned";
                var per = ((double) ar.RiskAssessment / total) * 100;

                chartData3[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData3, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPercentageImpactCategoryRiskDataRRB(string risk)
        {

            var query = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).GroupBy(ara => ara.RiskImpactCategory).Select(d => new
            {
                Category = d.Key,
                RiskAssessment = d.Count()
            });
            var total = db.RiskAssessments.Where(r => r.Approval == true && r.Status != null && r.Status != "Close" && r.RiskImpactCategory != null).Count();
            var chartData3 = new object[query.Count() + 1];
            chartData3[0] = new object[]{
                "Impact Category",
                "% Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Category ?? "Unassigned";
                var per = ((double)ar.RiskAssessment / total) * 100;

                chartData3[j] = new object[] { sts, Math.Round(per,1) };



            }
            return Json(chartData3, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetImpactLevelARiskData(string risk)
        {
            //await Task.Delay(10);
            var query = db.RiskAssessments.Where(r => r.Risk == risk && r.Approval == true && r.Status != null && r.Status != "Close" && r.ImpactLevel !=null).GroupBy(ara => ara.ImpactLevel).Select(d => new
            {
                Il = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData4 = new object[query.Count() + 1];
            chartData4[0] = new object[]{
                "Impact Level",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query)
            {
                j++;
                string sts = ar.Il ?? "Unassigned";

                chartData4[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData4, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTargetQuarterARiskData(string risk)
        {
            //await Task.Delay(10);
            var query5 = db.RiskAssessments.Where(r => r.Risk == risk && r.Approval == true && r.Status != null && r.Status != "Close" && r.TargetQuarter!=null).GroupBy(ara => ara.TargetQuarter).Select(d => new
            {
                Tq = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData5 = new object[query5.Count() + 1];
            chartData5[0] = new object[]{
                "Quarter",
                "Number of Risks"
            };
            var j = 0;
            foreach (var ar in query5)
            {
                j++;
                string sts = ar.Tq ?? "Unassigned";

                chartData5[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData5, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTargetQuarterRRBData(string risk)
        {
            //await Task.Delay(10);
            var query5 = db.RiskAssessments.Where(r =>  r.Approval == true && r.Status != null && r.Status != "Close" && r.TargetQuarter != null).GroupBy(ara => ara.TargetQuarter).Select(d => new
            {
                Tq = d.Key,
                RiskAssessment = d.Count()
            });
            var chartData5 = new object[query5.Count() + 1];
            chartData5[0] = new object[]{
                "Quarter",
                "# of Targets"
            };
            var j = 0;
            foreach (var ar in query5)
            {
                j++;
                string sts = ar.Tq ?? "Unassigned";

                chartData5[j] = new object[] { sts, ar.RiskAssessment };



            }
            return Json(chartData5, JsonRequestBehavior.AllowGet);
        }

    }
}