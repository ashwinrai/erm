﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Web.Security;
using System.Security.Principal;
using System.DirectoryServices.ActiveDirectory;
using System.Configuration;

namespace RMS_HIPAA.Controllers
{
    public class HomeController : Controller
    {
        public List<string> AdGroupNames;

        //[Authorize(Roles = @"HealthDoxCloud\IndidgeGroup")]
        //[Authorize(Roles = "RMS_Admin,RMS_Technology,IndidgeGroup")]
        public ActionResult Index()
        {
            //ViewBag.Title = "AD Groups";
            //string domainName = System.Configuration.ConfigurationManager.AppSettings["AMServer"];	// Domain name can be Read from Config file or from the current application

            //if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    AdGroupNames = new List<string>();
            //    UserPrincipal user = UserPrincipal.FindByIdentity(new PrincipalContext(ContextType.Domain, domainName), System.Web.HttpContext.Current.User.Identity.Name);
            //    foreach (GroupPrincipal group in user.GetGroups())
            //    {
            //        AdGroupNames.Add(group.Name);
            //    }
            //}

            //string adGrpsStr = JsonConvert.SerializeObject(AdGroupNames);
            //TempData["AdGroups"] = adGrpsStr;
            //ViewBag.AdGroups = adGrpsStr;
            return View();
        }

        public ActionResult HomeView()
        {
            return View();
        }
    }
}
