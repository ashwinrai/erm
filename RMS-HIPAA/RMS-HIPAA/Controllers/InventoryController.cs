﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RMS_HIPAA.Models.AssetInventoryModel;

namespace RMS_HIPAA.Controllers
{
    public class InventoryController : Controller
    {
        // GET: Inventory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SoftwareInventoryView()
        {
            return View();
        }

        public ActionResult HardwareInventoryView()
        {
            return View();
        }

        public async Task<PartialViewResult> _addSWItem()
        {
            var model = new SoftwareInventoryViewModel();
            await model.InitializeValues();
            return PartialView(model);
        }
        public async Task<PartialViewResult> _editSWItem(int id)
        {
            var model = new SoftwareInventoryViewModel();
            await model.InitializeValues();
            return PartialView(model);
        }

        public async Task<PartialViewResult> _addHWItem()
        {
            var model = new HardwareInventoryViewModel();
            await model.InitializeValues();
            return PartialView(model);
        }
        public async Task<PartialViewResult> _editHWItem(int id)
        {
            var model = new HardwareInventoryViewModel();
            await model.InitializeValues();
            return PartialView(model);
        }
    }
}