﻿using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RMS_HIPAA.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalRiskAssessmentReport()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalRiskAssessmentPDFReport()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalRiskAssessmentKPIReport()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalSelfRiskAssessmentReport()
        {
            SelfRiskAssessmentReportViewModel SARView = new SelfRiskAssessmentReportViewModel();
            SARView.FromDate = DateTime.Now.ToString("MM/dd/yyyy").Replace('-', '/');
            return View(SARView);
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeRiskAssessmentReport()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeRiskAssessmentPDFReport()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeRiskAssessmentKPIReport()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeSelfRiskAssessmentReport()
        {
           SelfRiskAssessmentReportViewModel SARView = new SelfRiskAssessmentReportViewModel();
            SARView.FromDate = DateTime.Now.ToString("MM/dd/yyyy").Replace('-', '/');
            return View(SARView);
        }
        

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologyRiskAssessmentReport()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologyRiskAssessmentPDFReport()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologyRiskAssessmentKPIReport()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologySelfRiskAssessmentReport()
        {
            SelfRiskAssessmentReportViewModel SARView = new SelfRiskAssessmentReportViewModel();
            SARView.FromDate = DateTime.Now.ToString("MM/dd/yyyy").Replace('-', '/');
            return View(SARView);
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult SoftwareAssetInventoryReport()
        {
            return View();
        }
       
        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult HardwareAssetInventoryReport()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult RiskAssessmentReport()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult RiskAssessmentPDFReport()
        {
            return View();
        }

        [Authorize(Roles = "Risk_Officers")]
        public ActionResult RiskAssessmentKRIReport()
        {
            return View();
        }
    }
}