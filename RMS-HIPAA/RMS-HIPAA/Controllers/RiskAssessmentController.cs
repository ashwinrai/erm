﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

using RMS_HIPAA.DAL;
using RMS_HIPAA.Models;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Framework.Log;


namespace RMS_HIPAA.Controllers
{
    public class RiskAssessmentController : Controller
    {
        private RMSHIPAAEntities db = new RMSHIPAAEntities();

        // GET: NewAdministrativeRiskAssessment
        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public async Task<ActionResult> NewAdministartiveRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Administrative");            
            return View(model);
        }

        // Get: EditAdministrativeRiskAssessment
        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public async Task<ActionResult> EditAdministrativeRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Administrative");
            return View(model);
        }

        //Administrative Risk Assessment Grid View
        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AllAdministrativeRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult OpenAdministrativeRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult InProgressAdministrativeRiskAssessment()
        {
            return View();
        }

        // GET: NewPhysicalRiskAssessment
        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public async Task<ActionResult> NewPhysicalRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Physical");           
            return View(model);
        }

        // Get: EditPhysicalRiskAssessment
        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public async Task<ActionResult> EditPhysicalRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Physical");
            return View(model);
        }

        //Physical Risk Assessment Grid View
        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult AllPhysicalRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult OpenPhysicalRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult InProgressPhysicalRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult ClosedAdministrativeRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult ClosedPhysicalRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult ClosedTechnologyRiskAssessment()
        {
            return View();
        }


        // GET: NewTechnologyRiskAssessment
        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public async Task<ActionResult> NewTechnologyRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Technology");          
            return View(model);
        }

        // Get: EditTechnologyRiskAssessment
        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public async Task<ActionResult> EditTechnologyRiskAssessment()
        {
            var model = new RiskAssessmentViewModel();
            await model.InitializeValues("Technology");
            return View(model);
        }

        //Technology Risk Assessment Grid View
        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologyRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult AllTechnologyRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult OpenTechnologyRiskAssessment()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult InProgressTechnologyRiskAssessment()
        {
            return View();
        }        


        public ActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Compliance_Manager,Risk_Officers")]
        public ActionResult AdministrativeRiskAssessmentKPI()
        {
            return View();
        }

        [Authorize(Roles = "Business_Function_Owner,Risk_Officers")]
        public ActionResult PhysicalRiskAssessmentKPI()
        {
            return View();
        }

        [Authorize(Roles = "IT_PMO_Owner,Risk_Officers")]
        public ActionResult TechnologyRiskAssessmentKPI()
        {
            return View();
        }


    }
}