﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;

namespace RMS_HIPAA.Controllers
{
    public class SelfRiskAssessmentController : Controller
    {
        // GET: SelfRiskAssessment
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AdministrativeSelfRiskAssessmentQuestions()
        {
            return View();
        }

        public ActionResult PhysicalSelfRiskAssessmentQuestions()
        {
            return View();
        }

        public ActionResult TechnologySelfRiskAssessmentQuestions()
        {
            return View();
        }

        public async Task<PartialViewResult> _editSelfRiskAssessmentQuestion(int id)
        {
            var model = new SelfRiskAssessmentQuestionsViewModel();
            return PartialView(model);
        }

        public async Task<PartialViewResult> _addSelfRiskAssessmentQuestion(string FunctionName)
        {
            var model = new SelfRiskAssessmentQuestionsViewModel();
            model.RiskType = FunctionName;
            return PartialView(model);
        }
    }
}