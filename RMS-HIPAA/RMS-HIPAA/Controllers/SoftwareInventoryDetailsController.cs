﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RMS_HIPAA.Repository;
using RMS_HIPAA.Models;
using RMS_HIPAA.Models.AssetInventoryModel;
using System.Collections.Specialized;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Controllers
{
    [Route("api/[SoftwareInventoryDetails]")]
    public class SoftwareInventoryDetailsController : ApiController
    {
        public ISoftwareInventoryRepository<SoftwareInventoryViewModel, int, int> _repository = null;

        public SoftwareInventoryDetailsController(ISoftwareInventoryRepository<SoftwareInventoryViewModel, int, int> repo)
        {
            this._repository = repo;
        }

        public SoftwareInventoryDetailsController()
        {
            this._repository = new SoftwareInventoryRepository();
        }

        [System.Web.Http.HttpGet]
        public SysDataTablePager Get()
        {
            var model = _repository.Get();

            IEnumerable<SoftwareInventoryViewModel> filteredmodels;

            NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query);
            string sEcho = nvc["sEcho"].ToString();
            int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
            int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);
            string sSearch = nvc["sSearch"].ToString();
            if (!string.IsNullOrEmpty(sSearch))
            {
                //Used if particulare columns are filtered 
                var AssetIdFilter = Convert.ToString(nvc["sSearch_1"]);
                var AssetClassFilter= Convert.ToString(nvc["sSearch_2"]);
                var ApplicationNameFilter = Convert.ToString(nvc["sSearch_3"]);
                var DescriptionFilter = Convert.ToString(nvc["sSearch_4"]);
                var OSFilter = Convert.ToString(nvc["sSearch_5"]);
                var SharedFilter = Convert.ToString(nvc["sSearch_6"]);
                var ApplicationCriticalityFilter = Convert.ToString(nvc["sSearch_7"]);
                var DataClassificationFilter = Convert.ToString(nvc["sSearch_8"]);
                var RiskassessfrequenceyFilter = Convert.ToString(nvc["sSearch_9"]);
                var BusinessFunctionFilter = Convert.ToString(nvc["sSearch_10"]);
                var BusinessOwnerFilter = Convert.ToString(nvc["sSearch_11"]);
                var DepartmentFilter = Convert.ToString(nvc["sSearch_12"]);

                //Optionally check whether the columns are searchable at all 
                var isAssetIdSearchable = Convert.ToBoolean(nvc["bSearchable_1"]);
                var isAssetClassSearchable = Convert.ToBoolean(nvc["bSearchable_2"]);
                var isApplicationNameSearchable = Convert.ToBoolean(nvc["bSearchable_3"]);
                var isDescriptionSearchable = Convert.ToBoolean(nvc["bSearchable_4"]);
                var isOSSearchable = Convert.ToBoolean(nvc["bSearchable_5"]);
                var isSharedSearchable = Convert.ToBoolean(nvc["bSearchable_6"]);
                var isApplicationCriticalitySearchable = Convert.ToBoolean(nvc["bSearchable_7"]);
                var isDataClassificationSearchable = Convert.ToBoolean(nvc["bSearchable_8"]);
                var isRiskassessfrequenceySearchable = Convert.ToBoolean(nvc["bSearchable_9"]);
                var isBusinessFunctionSearchable = Convert.ToBoolean(nvc["bSearchable_10"]);
                var isBusinessOwnerSearchable = Convert.ToBoolean(nvc["bSearchable_11"]);
                var isDepartmentSearchable = Convert.ToBoolean(nvc["bSearchable_12"]);

                filteredmodels = model
                    .Where(c => isAssetIdSearchable && c.AssetId.ToLower().Contains(sSearch.ToLower())||
                     isAssetClassSearchable && (( c.AssetClass!= null) ? c.AssetClass.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isApplicationNameSearchable && ((c.ApplicationName != null) ? c.ApplicationName.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isDescriptionSearchable&& ((c.Description != null) ? c.Description.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isOSSearchable && ((c.Os != null) ? c.Os.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isSharedSearchable && ((c.Shared != null) ? c.Shared.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isApplicationCriticalitySearchable && ((c.ApplicationCriticality != null) ? c.ApplicationCriticality.ToString().ToLower().Contains(sSearch.ToLower()) : false) ||
                     isDataClassificationSearchable && ((c.DataClassification != null) ? c.DataClassification.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isRiskassessfrequenceySearchable && ((c.RiskAssessFreq != null) ? c.RiskAssessFreq.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isBusinessFunctionSearchable && ((c.BusinessFunction != null) ? c.BusinessFunction.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isBusinessOwnerSearchable && ((c.BusinessOwnerName != null) ? c.BusinessOwnerName.ToString().ToLower().Contains(sSearch.ToLower()) : false)||
                     isDepartmentSearchable && ((c.Department != null) ? c.Department.ToString().ToLower().Contains(sSearch.ToLower()) : false)
                     );
            }
            else
            {
                filteredmodels = model;
            }

            var isAssetIdSortable = Convert.ToBoolean(nvc["bSortable_1"]);
            var isAssetClassSortable = Convert.ToBoolean(nvc["bSortable_2"]);
            var isApplicationNameSortable = Convert.ToBoolean(nvc["bSortable_3"]);
            var isDescriptionSortable = Convert.ToBoolean(nvc["bSortable_4"]);
            var isOSSortable = Convert.ToBoolean(nvc["bSortable_5"]);
            var isSharedSortable = Convert.ToBoolean(nvc["bSortable_6"]);
            var isApplicationCriticalitySortable = Convert.ToBoolean(nvc["bSortable_7"]);
            var isDataClassificationSortable = Convert.ToBoolean(nvc["bSortable_8"]);
            var isRiskassessfrequenceySortable = Convert.ToBoolean(nvc["bSortable_9"]);
            var isBusinessFunctionSortable = Convert.ToBoolean(nvc["bSortable_10"]);
            var isBusinessOwnerSortable = Convert.ToBoolean(nvc["bSortable_11"]);
            var isDepartmentSortable = Convert.ToBoolean(nvc["bSortable_12"]);

            var sortColumnIndex = Convert.ToInt32(nvc["iSortCol_0"]);
            Func<SoftwareInventoryViewModel, string> orderingFunction = (c =>
                                                                    sortColumnIndex == 1 && isAssetIdSortable ? c.AssetId.ToString() :
                                                                    sortColumnIndex == 2 && isAssetClassSortable ? c.AssetClass :
                                                                    sortColumnIndex == 3 && isApplicationNameSortable? c.ApplicationName.ToString() :
                                                                    sortColumnIndex == 4 && isDescriptionSortable ? c.Description :
                                                                    sortColumnIndex == 5 && isOSSortable ? c.Os :
                                                                    sortColumnIndex == 6 && isSharedSortable ? c.Shared :
                                                                    sortColumnIndex == 7 && isApplicationCriticalitySortable ? c.ApplicationName :
                                                                    sortColumnIndex == 8 && isDataClassificationSortable ? c.DataClassification :
                                                                    sortColumnIndex == 9 && isRiskassessfrequenceySortable ? c.RiskAssessFreq :
                                                                    sortColumnIndex == 10 && isBusinessFunctionSortable ? c.BusinessFunction :
                                                                    sortColumnIndex == 11 && isBusinessOwnerSortable ? c.BusinessOwnerName :
                                                                    sortColumnIndex == 12 && isDepartmentSortable ? c.Department :
                                                                   "");



            var sortDirection = nvc["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredmodels = filteredmodels.OrderBy(orderingFunction);
            else
                filteredmodels = filteredmodels.OrderByDescending(orderingFunction);

            var displayedtechnology = filteredmodels.Skip(iDisplayStart).Take(iDisplayLength);



            var CustomerPaged = new Models.SysDataTablePager();
            CustomerPaged.sEcho = sEcho;
            CustomerPaged.iTotalRecords = filteredmodels.Count();
            CustomerPaged.iTotalDisplayRecords = filteredmodels.Count();
            var result = from c in displayedtechnology select new[] { Convert.ToString(c.Id),Convert.ToString(c.AssetId), c.AssetClass, c.ApplicationName, c.Description, c.Os, c.Shared, c.ApplicationCriticality, c.DataClassification, c.RiskAssessFreq, c.BusinessFunction, c.BusinessOwnerName, c.Department, "actions" };
            CustomerPaged.aaData = result.ToList();

            return CustomerPaged;
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var model = _repository.Get(id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpPost]
        public IHttpActionResult Post(SoftwareInventoryViewModel SWIDetails)
        {
            try
            {
                _repository.Add(SWIDetails);

                return Ok<string>("Success");
            }
            catch (DbEntityValidationException ex)
            {
                return InternalServerError(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]SoftwareInventoryViewModel SWIDetails)
        {
            try
            {
                _repository.Update(SWIDetails);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


    }
}
