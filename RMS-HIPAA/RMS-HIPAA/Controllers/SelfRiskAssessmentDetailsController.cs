﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using RMS_HIPAA.Repository;
using System.Data.Entity.Validation;
using RMS_HIPAA.Framework.Service;
using RMS_HIPAA.Models;
using System.Collections.Specialized;

namespace RMS_HIPAA.Controllers
{
    [Route("api/[SelfRiskAssessmentDetails]")]
    public class SelfRiskAssessmentDetailsController : ApiController
    {
        public ISelfRiskAssessmentQuestionsRepository<SelfRiskAssessmentQuestionsViewModel, int> _repository = null;

        //The Dependency Injection of the IRepository<TEnt, in TPk>
        public SelfRiskAssessmentDetailsController(ISelfRiskAssessmentQuestionsRepository<SelfRiskAssessmentQuestionsViewModel, int> repo)
        {
            this._repository = repo;
        }

        public SelfRiskAssessmentDetailsController()
        {
            this._repository = new SelfRiskAssessmentQuestionsRepository();
        }

        [System.Web.Http.HttpGet]
        public SysDataTablePager Get(string FunctionName)
        {
            var model = _repository.Get(FunctionName);
          
            model = model.ToList();

            IEnumerable<SelfRiskAssessmentQuestionsViewModel> filteredmodels;

            NameValueCollection nvc = System.Web.HttpUtility.ParseQueryString(Request.RequestUri.Query);
            string sEcho = nvc["sEcho"].ToString();
            int iDisplayStart = Convert.ToInt32(nvc["iDisplayStart"]);
            int iDisplayLength = Convert.ToInt32(nvc["iDisplayLength"]);
            string sSearch = nvc["sSearch"].ToString();
            if (!string.IsNullOrEmpty(sSearch))
            {
                //Used if particulare columns are filtered  


                var QuestionsFilter = Convert.ToString(nvc["sSearch_3"]);

                //Optionally check whether the columns are searchable at all 

                var isQuestionsSearchable = Convert.ToBoolean(nvc["bSearchable_3"]);

                filteredmodels = model
               .Where(c =>
                           isQuestionsSearchable && c.Questions.ToLower().Contains(sSearch.ToLower()));
            }
            else
            {
                filteredmodels = model;
            }

            var isQuestionsSortable = Convert.ToBoolean(nvc["bSortable_3"]);


            var sortColumnIndex = Convert.ToInt32(nvc["iSortCol_0"]);
            Func<SelfRiskAssessmentQuestionsViewModel, string> orderingFunction = (c =>
                                                                    sortColumnIndex == 3 && isQuestionsSortable ? c.Questions : "");

            var sortDirection = nvc["sSortDir_0"]; // asc or desc
            if (sortDirection == "asc")
                filteredmodels = filteredmodels.OrderBy(orderingFunction);
            else
                filteredmodels = filteredmodels.OrderByDescending(orderingFunction);

            var displayedtechnology = filteredmodels.Skip(iDisplayStart).Take(iDisplayLength);



            var CustomerPaged = new Models.SysDataTablePager();

            CustomerPaged.sEcho = sEcho;
            CustomerPaged.iTotalRecords = filteredmodels.Count();
            CustomerPaged.iTotalDisplayRecords = filteredmodels.Count();

            var result = from c in displayedtechnology select new[] { c.Id.ToString(), c.RiskType, c.SINo.ToString(), c.Questions, "Actions" };
            CustomerPaged.aaData = result.ToList();

            return CustomerPaged;

        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var model = _repository.Get(id);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [HttpPut]
        public IHttpActionResult Put([FromBody]SelfRiskAssessmentQuestionsViewModel RiskAssess)
        {
            try
            {
                _repository.Update(RiskAssess);
                return Ok<int>(RiskAssess.Id);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpPost]
        public IHttpActionResult Post(SelfRiskAssessmentQuestionsViewModel RiskAssess)
        {
            try
            {
                _repository.Add(RiskAssess);
                // string RiskId = RiskAssessmentService.GenerateRiskId(RiskAssess.Risk);
                return Ok<string>("");
            }
            catch (DbEntityValidationException ex)
            {
                return InternalServerError(ex);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
                return Ok<string>("Success");
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
