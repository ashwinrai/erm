﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class SelfRiskAssessmentTestRepository : ISelfRiskAssessmentTestRepository<SelfAssessmentQuestionTestViewModel, int>
    {
        //[Dependencey]
        public RMSHIPAAEntities db = null;

        public SelfRiskAssessmentTestRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public SelfRiskAssessmentTestRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        //public IEnumerable<SelfAssessmentQuestionTestViewModel> Get()
        //{
        //    var scho = db.SelfRiskAssessmentQuestions.ToList();
        //    var model = scho.Select(x => new SelfAssessmentQuestionTestViewModel(x)).ToList();
        //    return model;
        //}

        public SelfAssessmentQuestionTestViewModel Get(string accesseddate, string FunctionName)
        {
            SelfAssessmentQuestionTestViewModel SQT = new SelfAssessmentQuestionTestViewModel();
            var model = SQT.ConvertToQuestionView(accesseddate, FunctionName);
            return model;
        }

        public void Add(SelfAssessmentQuestionTestViewModel SelfRiskAssess)
        {
            try
            {
                if (SelfRiskAssess.Id == 0)
                {
                    SelfRiskAssessmentTest SAT = new SelfRiskAssessmentTest();
                    SAT.AssessorName = SelfRiskAssess.AssessorName;
                    SAT.AssessedDate = SelfRiskAssess.AssessedDate;
                    SAT.RiskType = SelfRiskAssess.RiskType;
                    SAT.CreatedBy = "Indidge";
                    SAT.CreatedOn = DateTime.Now;
                    db.SelfRiskAssessmentTests.Add(SAT);

                    foreach (SelfAssessmentQuestionTestDetailsViewModel SQTD in SelfRiskAssess.QuestionViewList)
                    {
                        SelfRiskAssessmentTestDetail SRTD = new SelfRiskAssessmentTestDetail();
                        SRTD.SelfRiskAssessmentTest = SAT;
                        SRTD.QuestionId = SQTD.QuestionId;
                        SRTD.Response = SQTD.Response;
                        SRTD.Comments = (SQTD.Comments.Trim() == "" || SQTD.Comments == null) ? "" : SQTD.Comments;
                        db.SelfRiskAssessmentTestDetails.Add(SRTD);
                    }
                }
                else
                {
                    SelfRiskAssessmentTest SAT = db.SelfRiskAssessmentTests.Where(x => x.Id == SelfRiskAssess.Id).FirstOrDefault();
                    SAT.AssessorName = SelfRiskAssess.AssessorName;
                    SAT.AssessedDate = SelfRiskAssess.AssessedDate;

                    //Delete previous values
                    db.SelfRiskAssessmentTestDetails.Where(y => y.TestRefId == SelfRiskAssess.Id).ToList().ForEach(y => db.SelfRiskAssessmentTestDetails.Remove(y));

                    //Add the self risk assessment test record
                    foreach (SelfAssessmentQuestionTestDetailsViewModel SQTD in SelfRiskAssess.QuestionViewList)
                    {
                        SelfRiskAssessmentTestDetail SRTD = new SelfRiskAssessmentTestDetail();
                        SRTD.TestRefId = SelfRiskAssess.Id;
                        SRTD.QuestionId = SQTD.QuestionId;
                        SRTD.Response = SQTD.Response;
                        SRTD.Comments = SQTD.Comments;
                        db.SelfRiskAssessmentTestDetails.Add(SRTD);
                    }
                }
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex1)
            {
            }
        }
    }
}