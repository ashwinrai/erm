﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class SelfRiskAssessmentQuestionsRepository : ISelfRiskAssessmentQuestionsRepository<SelfRiskAssessmentQuestionsViewModel, int>
    {
        //[Dependencey]
        public RMSHIPAAEntities db = null;

        public SelfRiskAssessmentQuestionsRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public SelfRiskAssessmentQuestionsRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        public IEnumerable<SelfRiskAssessmentQuestionsViewModel> Get()
        {
            var scho = db.SelfRiskAssessmentQuestions.ToList();
            int j = 1;
            var model = scho.Select(x => new SelfRiskAssessmentQuestionsViewModel(x, j++)).ToList();
            return model;
        }

        public IEnumerable<SelfRiskAssessmentQuestionsViewModel> Get(string FunctionName)
        {
            var scho = db.SelfRiskAssessmentQuestions.Where(x => x.RiskType == FunctionName).ToList();
            int j = 1;
            var model = scho.Select(x => new SelfRiskAssessmentQuestionsViewModel(x, j++)).ToList();
            return model;
        }

        public SelfRiskAssessmentQuestionsViewModel Get(int id)
        {
            var selfRiskAssess = db.SelfRiskAssessmentQuestions.Where(x => x.Id == id).FirstOrDefault();

            var model = new SelfRiskAssessmentQuestionsViewModel();

            model = model.ConvertFromSelfRiskAssessmentQuestions(selfRiskAssess);

            return model;
        }

        public void Add(SelfRiskAssessmentQuestionsViewModel RiskAssess)
        {
            try
            {
                var RskAssess = RiskAssess.ConvertToSelfRiskAssessmentQuestions();
                db.SelfRiskAssessmentQuestions.Add(RskAssess);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex1)
            {
            }
        }

        public void Update(SelfRiskAssessmentQuestionsViewModel RiskAssess)
        {
            var aRa = db.SelfRiskAssessmentQuestions.Find(RiskAssess.Id);
            if (aRa != null)
            {
                aRa.Question = RiskAssess.Questions;
                aRa.ModifiedBy = HttpContext.Current.User.Identity.Name;
                aRa.ModifiedOn = DateTime.Now;
                db.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            var obj = db.SelfRiskAssessmentQuestions.Find(id);
            db.SelfRiskAssessmentQuestions.Remove(obj);
            db.SaveChanges();
        }
    }
}