﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.AssetInventoryModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class HardwareInventoryRepository : IHardwareInventoryRepository<HardwareInventoryViewModel, int, int>
    {
        public RMSHIPAAEntities db = null;

        public HardwareInventoryRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public HardwareInventoryRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        public IEnumerable<HardwareInventoryViewModel> Get()
        {
            var scho = db.HardwareInventories.ToList();
            var model = scho.Select(x => new HardwareInventoryViewModel(x)).ToList();
            return model;
        }

        public HardwareInventoryViewModel Get(int id)
        {
            var hardwareIn = db.HardwareInventories.Where(x => x.Id == id).FirstOrDefault();

            var model = new HardwareInventoryViewModel();

            model = model.ConvertFromSoftwareInventory(hardwareIn);

            return model;
        }

        public void Add(HardwareInventoryViewModel HardwareIn)
        {
            try
            {
                var SI = HardwareIn.ConvertToHardwareInventory();
                db.HardwareInventories.Add(SI);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex1)
            {
            }
        }

        public void Update(HardwareInventoryViewModel SID)
        {
            var SI = db.HardwareInventories.Find(SID.Id);
            if (SI != null)
            {
                SI.AssetId = SID.AssetId;
                SI.AssetClass = SID.AssetClass;
                SI.SystemName = SID.SystemName;
                SI.Description = SID.Description;
                SI.Model = SID.Model;
                SI.Type = SID.Type;
                SI.HardwareCriticality = SID.HardwareCriticality;
                
                SI.RiskAssessFrequencey = SID.RiskAssessFreq;
                SI.BusinessFunction = SID.BusinessFunction;
                SI.BusinessOwner = SID.BusinessOwnerName;
                SI.Department = SID.Department;
                SI.ModifiedBy = "Indidge";
                SI.ModifiedOn = DateTime.UtcNow;

                db.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            try
            {
                var obj = db.HardwareInventories.Find(id);
                db.HardwareInventories.Remove(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }
    }
}