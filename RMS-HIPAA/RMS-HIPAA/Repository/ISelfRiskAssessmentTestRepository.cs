﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Repository
{
    public interface ISelfRiskAssessmentTestRepository<AEnt, in APk> where AEnt : class
    {
        //IEnumerable<AEnt> Get();
        AEnt Get(string accesseddate, string FunctionName);
        //AEnt Get(int id);
         void Add(AEnt obj);
        //void Update(AEnt obj);
        //void Delete(int id);
    }
}