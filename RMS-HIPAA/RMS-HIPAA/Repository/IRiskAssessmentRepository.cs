﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Repository
{
    public interface IRiskAssessmentRepository<AEnt, in APk, in Guid> where AEnt : class
    {
        IEnumerable<AEnt> Get();
        IEnumerable<AEnt> Get(string FunctionName);
        AEnt Get(Guid id);
        void Add(AEnt obj);
        void Update(AEnt obj);
        void Delete(Guid id);
    }
}