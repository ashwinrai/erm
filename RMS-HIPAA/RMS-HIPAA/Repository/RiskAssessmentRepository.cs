﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class RiskAssessmentRepository: IRiskAssessmentRepository<RiskAssessmentViewModel, int, Guid>
    {

        //[Dependencey]
        public RMSHIPAAEntities db = null;

        public RiskAssessmentRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public RiskAssessmentRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        public IEnumerable<RiskAssessmentViewModel> Get()
        {
            var scho = db.RiskAssessments.ToList();
            var model = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
            return model;
        }

        public IEnumerable<RiskAssessmentViewModel> Get(string FunctionName)
        {
            var scho = db.RiskAssessments.Where(x=>x.Risk == FunctionName).OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
            var model = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
            return model;
        }

        public RiskAssessmentViewModel Get(Guid id)
        {
            var RiskAssess = db.RiskAssessments.Where(x => x.Id == id).FirstOrDefault();

            var model = new RiskAssessmentViewModel();

            model = model.ConvertFromRiskAssessment(RiskAssess);

            return model;
        }

        public void Add(RiskAssessmentViewModel RiskAssess)
        {
            try
            {
                var RskAssess = RiskAssess.ConvertToRiskAssessment();
                db.RiskAssessments.Add(RskAssess);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex1)
            {
            }
        }

        public void Update(RiskAssessmentViewModel RiskAssess)
        {
            var aRa = db.RiskAssessments.Find(RiskAssess.RiskAssessmentId);
            if (aRa != null)
            {
                if (RiskAssess.performPIR != "true")
                {
                    aRa.RiskAgent = RiskAssess.RiskAgent;
                    aRa.DepartmentorFunctionName = RiskAssess.DepartmentName;
                    aRa.AssetName = RiskAssess.AssetName;
                    aRa.AssessedDate = RiskAssess.AssessedDate;
                    aRa.RiskDescription = RiskAssess.RiskDescription;
                    aRa.AssessorOwner = RiskAssess.Owner;
                    aRa.ThreatVulnerabilityDescription = RiskAssess.ThreatVulnerabilityDescription;
                    aRa.RiskInput = RiskAssess.RiskInput;
                    aRa.ThreatSource = RiskAssess.ThreatSource;
                    aRa.RiskImpactCategory = RiskAssess.RiskImpactCategory;
                    aRa.ProbabilityofOccurence = RiskAssess.ProbabilityofOccurenceValue;
                    aRa.ImpactLevel = RiskAssess.ImpactLevelValue;
                    aRa.RiskExposureLevel = RiskAssess.RiskExposureLevel;
                    aRa.ExistingControl = RiskAssess.ExistingControls;
                    aRa.NewProposedControl = RiskAssess.ActionProposed;
                    aRa.Responsible = RiskAssess.Responsible;
                    aRa.TargetDateImplement = RiskAssess.TargetDate;
                    aRa.TargetQuarter = RiskAssess.TargetQurter;
                    aRa.Department = RiskAssess.ResponsibleDepartment;
                    aRa.RiskResponse = RiskAssess.RiskResponse;
                    aRa.Status = RiskAssess.Status;
                    aRa.ClosedDate = RiskAssess.ClosedDate;
                    aRa.SentToApproval = RiskAssess.SentToApproval;
                    aRa.ModifiedBy = HttpContext.Current.User.Identity.Name;
                    aRa.ModifiedOn = DateTime.Now;
                }

                else if (RiskAssess.performPIR == "true")
                {
                    aRa.PIRProbabilityOfOccurence = RiskAssess.PIRProbabilityofOccurenceValue;
                    aRa.PIRImpactLevel = RiskAssess.PIRImpactLevelValue;
                    aRa.PIRRiskExposureLevel = RiskAssess.PIRRiskExposureLevel;
                    aRa.PIRComments = RiskAssess.PIRComments;
                    aRa.PIRCreatedBy = HttpContext.Current.User.Identity.Name;
                    aRa.PIRCreatedOn = DateTime.UtcNow;
                }
                db.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            var obj = db.RiskAssessments.Find(id);
            db.RiskAssessments.Remove(obj);
            db.SaveChanges();
        }

    }
}