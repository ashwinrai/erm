﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class RiskReviewBoardRepository : IRiskReviewBoardRepository<RiskAssessmentViewModel, int, Guid>
    {
        //[Dependencey]
        public RMSHIPAAEntities db = null;

        public RiskReviewBoardRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public RiskReviewBoardRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        public IEnumerable<RiskAssessmentViewModel> Get()
        {
            var scho = db.RiskAssessments.ToList();
            var model = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
            return model;
        }

        public IEnumerable<RiskAssessmentViewModel> Get(string FunctionName)
        {
            if (FunctionName== "Approved")
            {
                var scho = db.RiskAssessments.Where(x => x.Approval == true).OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                var model = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                return model;
            }
            else
            {
                var scho = db.RiskAssessments.Where(x => x.Risk == FunctionName && x.SentToApproval == true && x.Status!="Close").OrderByDescending(x => x.ProbabilityofOccurence1.ProbabilityValue * x.ImpactLevel1.ImpactValue).ToList();
                var model = scho.Select(x => new RiskAssessmentViewModel(x)).ToList();
                return model;
            }
            
        }

        public RiskAssessmentViewModel Get(Guid id)
        {
            var RiskAssess = db.RiskAssessments.Where(x => x.Id == id).FirstOrDefault();

            var model = new RiskAssessmentViewModel();

            model = model.ConvertFromRiskAssessment(RiskAssess);

            return model;
        }

        public void Update(RiskAssessmentViewModel RiskAssess)
        {
            var aRa = db.RiskAssessments.Where(r => r.RiskId == RiskAssess.RiskId).SingleOrDefault();
            if (aRa != null)
            {
                aRa.RiskAgent = RiskAssess.RiskAgent;
                aRa.DepartmentorFunctionName = RiskAssess.DepartmentName;
                aRa.AssetName = RiskAssess.AssetName;
                aRa.AssessedDate = RiskAssess.AssessedDate;
                aRa.RiskDescription = RiskAssess.RiskDescription;
                aRa.AssessorOwner = RiskAssess.Owner;
                aRa.ThreatVulnerabilityDescription = RiskAssess.ThreatVulnerabilityDescription;
                aRa.RiskInput = RiskAssess.RiskInput;
                aRa.ThreatSource = RiskAssess.ThreatSource;
                aRa.RiskImpactCategory = RiskAssess.RiskImpactCategory;
                aRa.ProbabilityofOccurence = RiskAssess.ProbabilityofOccurence;
                aRa.ImpactLevel = RiskAssess.ImpactLevel;
                aRa.NewProposedControl = RiskAssess.ActionProposed;
                aRa.Responsible = RiskAssess.Responsible;
                aRa.TargetDateImplement = RiskAssess.TargetDate;
                aRa.TargetQuarter = RiskAssess.TargetQurter;
                aRa.Department = RiskAssess.ResponsibleDepartment;
                aRa.RiskResponse = RiskAssess.RiskResponse;
                aRa.Status = RiskAssess.Status;
                aRa.ClosedDate = RiskAssess.ClosedDate;
                aRa.ModifiedBy = HttpContext.Current.User.Identity.Name;
                aRa.ModifiedOn = DateTime.Now;

                db.SaveChanges();
            }
        }

        public void Approve(Guid id)
        {
            var aRa = db.RiskAssessments.Where(r => r.Id == id).SingleOrDefault();
            if (aRa != null)
            {
                aRa.SentToApproval = false;
                aRa.Approval = true;
                aRa.ApprovedOn = DateTime.UtcNow;
                aRa.ApprovedBy= HttpContext.Current.User.Identity.Name;
                aRa.ModifiedBy = HttpContext.Current.User.Identity.Name;
                aRa.ModifiedOn = DateTime.Now;

                db.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var obj = db.RiskAssessments.Find(id);
                db.RiskAssessments.Remove(obj);
                db.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }
    }
}