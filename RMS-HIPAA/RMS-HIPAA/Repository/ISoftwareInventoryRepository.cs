﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Repository
{
    public interface ISoftwareInventoryRepository<SEnt, in SPk, in Id>where SEnt:class
    {
        IEnumerable<SEnt> Get();
        SEnt Get(Id id);
        void Add(SEnt obj);
        void Update(SEnt obj);
        void Delete(Id id);
    }
}