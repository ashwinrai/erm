﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Repository
{
    public interface IRiskReviewBoardRepository<AEnt, in APk, in Guid> where AEnt : class
    {
        IEnumerable<AEnt> Get();
        IEnumerable<AEnt> Get(string FunctionName);
        AEnt Get(Guid id);
        void Update(AEnt obj);
        void Approve(Guid id);
        void Delete(Guid id);
    }
}