﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RMS_HIPAA.Repository
{
    public interface IHardwareInventoryRepository<HEnt, in HPk, in Id> where HEnt : class
    {
        IEnumerable<HEnt> Get();
        HEnt Get(Id id);
        void Add(HEnt obj);
        void Update(HEnt obj);
        void Delete(Id id);
    }
}