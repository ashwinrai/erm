﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RMS_HIPAA.Models.AssetInventoryModel;
using RMS_HIPAA.DAL;
using System.Data.Entity.Validation;

namespace RMS_HIPAA.Repository
{
    public class SoftwareInventoryRepository : ISoftwareInventoryRepository<SoftwareInventoryViewModel,int,int>
    {
        public RMSHIPAAEntities db = null;

        public SoftwareInventoryRepository()
        {
            db = new RMSHIPAAEntities();
        }

        public SoftwareInventoryRepository(RMSHIPAAEntities _db)
        {
            this.db = _db;
        }

        public IEnumerable<SoftwareInventoryViewModel> Get()
        {
            try
            {
                var scho = db.SoftwareInventories.ToList();
                var model = scho.Select(x => new SoftwareInventoryViewModel(x)).ToList();
                return model;
            }
           catch(Exception ex)
            {
                return null;
            }
        }

        public SoftwareInventoryViewModel Get(int id)
        {
            var softwareIn = db.SoftwareInventories.Where(x => x.Id == id).FirstOrDefault();

            var model = new SoftwareInventoryViewModel();

            model = model.ConvertFromSoftwareInventory(softwareIn);

            return model;
        }

        public void Add(SoftwareInventoryViewModel SoftwareIn)
        {
            try
            {
                var SI = SoftwareIn.ConvertToSoftwareInventory();
                db.SoftwareInventories.Add(SI);
                db.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                List<string> errorMessages = new List<string>();
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        errorMessages.Add(entityName + "." + error.PropertyName + ": " + error.ErrorMessage);
                    }
                }
            }
            catch (Exception ex1)
            {
            }
        }

        public void Update(SoftwareInventoryViewModel SID)
        {
            var SI = db.SoftwareInventories.Find(SID.Id);
            if(SI!=null)
            {
                SI.AssetId = SID.AssetId;
                SI.AssetClass = SID.AssetClass;
                SI.ApplicationName = SID.ApplicationName;
                SI.Description = SID.Description;
                SI.OperatingSystem = SID.Os;
                SI.Shared = SID.Shared;
                SI.ApplicationCriticality = SID.ApplicationCriticality;
                SI.DataClassification = SID.DataClassification;
                SI.RiskAssessFrequencey = SID.RiskAssessFreq;
                SI.BusinessFunction = SID.BusinessFunction;
                SI.BusinessOwnerName = SID.BusinessOwnerName;
                SI.Department = SID.Department;
                SI.ModifiedBy = "Indidge";
                SI.ModifiedOn = DateTime.UtcNow;

                db.SaveChanges();
            }
            
        }
        public void Delete(int id)
        {
            try
            {
                var obj = db.SoftwareInventories.Find(id);
                db.SoftwareInventories.Remove(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
        }
    }
}