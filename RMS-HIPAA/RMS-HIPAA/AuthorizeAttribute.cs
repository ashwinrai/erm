﻿using Newtonsoft.Json;
using RMS_HIPAA.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace RMS_HIPAA
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class AuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var isAuthorized = base.AuthorizeCore(httpContext);

            //Framework.Log.Logger.LogInfo("isauthorized" + isAuthorized);
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return false;

             //if (!isAuthorized) return false;

           

            //Framework.Log.Logger.LogInfo("current user1" + HttpContext.Current.User.Identity.Name);
            return true;
            //Framework.Log.Logger.LogInfo("current Roles" + Roles);
           
            HttpCookie authCookie = HttpContext.Current.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
            if (authCookie == null)
            {
                //Framework.Log.Logger.LogInfo("current user2" + HttpContext.Current.User.Identity.Name);
                CustomPrincipalSerializeModel serializeModel = ADManager.GetGroups();
                //Framework.Log.Logger.LogInfo("current user3" + HttpContext.Current.User.Identity.Name);
                var s = "";
                foreach (var r in serializeModel.roles)
                {
                    s = string.Join("|", r);

                }
                RMS_HIPAA.Framework.Log.Logger.LogError("Authenticate roles " + s);

                        string userData = JsonConvert.SerializeObject(serializeModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                                 1,
                                HttpContext.Current.User.Identity.Name,
                                 DateTime.Now,
                                 DateTime.Now.AddMinutes(15),
                                 false,
                                 userData);

                        string encTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                        // clear out existing cookie for good measure (probably overkill) then add
                        HttpContext.Current.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                        HttpContext.Current.Response.Cookies.Add(faCookie);
            
            //Assign userid,roles into current user context
            CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
            newUser.UserId = serializeModel.UserId;
            newUser.FirstName = serializeModel.FirstName;
            newUser.LastName = serializeModel.LastName;
            newUser.roles = serializeModel.roles;

            HttpContext.Current.User = newUser;
            //Framework.Log.Logger.LogInfo("cookiee value" + faCookie);
           }

            if (!String.IsNullOrEmpty(Roles))
            {
                string[] SysRoles = Roles.Split(',');
                string WebRoles = "";
                bool first = true;
                //Framework.Log.Logger.LogInfo("first role");
                foreach (string sRole in SysRoles)
                {
                    if (!first) WebRoles = WebRoles + ",";
                    string s2 = System.Configuration.ConfigurationManager.AppSettings[sRole];
                         //Framework.Log.Logger.LogInfo("first role1" + s2);
                    WebRoles = WebRoles + System.Configuration.ConfigurationManager.AppSettings[sRole].Trim();
                    first = false;
                }
                //RMS_HIPAA.Framework.Log.Logger.LogError("web roles " + WebRoles);
                if (!CurrentUser.IsInRole(WebRoles))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }

            //Framework.Log.Logger.LogInfo("Authiroize core" + HttpContext.Current.User.Identity.Name);

            //return base.AuthorizeCore(httpContext);
           return httpContext.Request.IsAuthenticated;
        }

        /// <summary>
        /// By Default, MVC returns a 401 Unauthorized when a user's roles do not meet the AuthorizeAttribute requirements.
        /// This initializes a reauthentication request to our identity provider.  Since the user is already logged in, 
        /// AAD returns to the same page, which then issues another 401, creating a redirect loop.
        /// Here, we override the AuthorizeAttribute's HandleUnauthorizedRequest method to show something that makes 
        /// sense in the context of our application.
        /// </summary>
        protected override void HandleUnauthorizedRequest(System.Web.Mvc.AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                //One Strategy:
                //filterContext.Result = new System.Web.Mvc.HttpStatusCodeResult((int)System.Net.HttpStatusCode.Forbidden);

                //Another Strategy:
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(
                        new
                        {
                            controller = "Error",
                            action = "AccessDenied"
                            //errorMessage = "You do not have sufficient priviliges to view this page."
                        })
                    );
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }
}