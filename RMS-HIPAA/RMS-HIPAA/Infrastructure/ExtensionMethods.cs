﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace RMS_HIPAA.Infrastructure
{
    public static class ExtensionMethods
    {
        public static IHtmlString UserDisplayName(this HtmlHelper helper, IIdentity identity)
        {
            try
            {
                string fullName = "";

                string domainName = System.Configuration.ConfigurationManager.AppSettings["AMServer"];
                if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    UserPrincipal user = UserPrincipal.FindByIdentity(new PrincipalContext(ContextType.Domain, domainName), System.Web.HttpContext.Current.User.Identity.Name);
                    if (user != null)
                    {
                        fullName = string.Format("{0} {1}", user.GivenName, user.Surname);
                    }

                }
                return new HtmlString(fullName);
            }
            catch (Exception)
            {
                return new HtmlString(string.Empty);
            }
        }
    }
}