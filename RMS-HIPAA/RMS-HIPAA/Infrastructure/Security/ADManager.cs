﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Principal;
using System.DirectoryServices.AccountManagement;

namespace RMS_HIPAA.Infrastructure.Security
{
    public class ADManager
    {
        public static CustomPrincipalSerializeModel GetGroups()
        {
            Framework.Log.Logger.LogInfo("AD Group" + HttpContext.Current.User.Identity.Name);
            string domainName = System.Configuration.ConfigurationManager.AppSettings["AMServer"];	// Domain name can be Read from Config file or from the current application

            List<string> AdGroupNames = new List<string>();
            CustomPrincipalSerializeModel CPS = new CustomPrincipalSerializeModel();
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {               
                UserPrincipal user = UserPrincipal.FindByIdentity(new PrincipalContext(ContextType.Domain, domainName), System.Web.HttpContext.Current.User.Identity.Name);
                CPS.UserId = user.UserPrincipalName;
                CPS.FirstName = user.GivenName;
                CPS.LastName = user.Surname;
                foreach (GroupPrincipal group in user.GetGroups())
                {
                    AdGroupNames.Add(group.Name);
                }
                CPS.roles = AdGroupNames.ToArray();
            }

            return CPS;
        }
    }
}