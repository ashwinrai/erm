using System;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using RMS_HIPAA.Models.RiskAssessmentModel;
using RMS_HIPAA.Models.AssetInventoryModel;
using RMS_HIPAA.Models.SelfRiskAssessmentModel;
using RMS_HIPAA.Repository;

namespace RMS_HIPAA
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            //Register the Repository in the Unity Container
            container.RegisterType<IRiskAssessmentRepository<RiskAssessmentViewModel, int, Guid>, RiskAssessmentRepository>();
            container.RegisterType<IRiskReviewBoardRepository<RiskAssessmentViewModel, int, Guid>, RiskReviewBoardRepository>();
            container.RegisterType<IHardwareInventoryRepository<HardwareInventoryViewModel, int, int>, HardwareInventoryRepository>();
            container.RegisterType<ISoftwareInventoryRepository<SoftwareInventoryViewModel, int, int>, SoftwareInventoryRepository>();
            container.RegisterType<ISelfRiskAssessmentQuestionsRepository<SelfRiskAssessmentQuestionsViewModel, int>, SelfRiskAssessmentQuestionsRepository>();
            container.RegisterType<ISelfRiskAssessmentTestRepository<SelfAssessmentQuestionTestViewModel, int>, SelfRiskAssessmentTestRepository>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}