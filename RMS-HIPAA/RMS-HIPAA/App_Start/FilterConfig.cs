﻿using System.Web;
using System.Web.Mvc;

namespace RMS_HIPAA
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
