﻿using System.Web;
using System.Web.Optimization;

namespace RMS_HIPAA
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new Bundle("~/bundles/jqueryJS").Include(
                "~/Scripts/jquery.js"));

            bundles.Add(new Bundle("~/bundles/jqueryPackage").Include(
                "~/Scripts/jquery-ui.js",
                "~/Scripts/jquery-ui.custom.js",
                "~/Scripts/jquery.ui.touch-punch.js",
                "~/Scripts/chosen.jquery.js",
                "~/Scripts/jquery.knob.js",
                "~/Scripts/jquery.inputlimiter.1.3.1.js",
                "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/jquery.toastmessage.js",
                "~/Scripts/dataTables/jquery.dataTables.js",
                "~/Scripts/dataTables/jquery.dataTables.bootstrap.js",
                "~/Scripts/dataTables/extensions/Select/js/dataTables.select.js"));

            bundles.Add(new Bundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js",
                "~/Scripts/date-time/moment.js",
                "~/Scripts/date-time/bootstrap-datepicker.js",
                "~/Scripts/date-time/bootstrap-timepicker.js",
                "~/Scripts/fuelux/fuelux.spinner.js",
                "~/Scripts/date-time/daterangepicker.js",
                "~/Scripts/date-time/bootstrap-datetimepicker.js",
                "~/Scripts/bootstrap-colorpicker.js",
                "~/Scripts/autosize.js",
                "~/Scripts/bootstrap-tag.js"));

            bundles.Add(new Bundle("~/bundles/ace").Include(
                "~/Scripts/ace/elements.scroller.js",
                "~/Scripts/ace/elements.colorpicker.js",
                "~/Scripts/ace/elements.fileinput.js",
                "~/Scripts/ace/elements.typeahead.js",
                "~/Scripts/ace/elements.wysiwyg.js",
                "~/Scripts/ace/elements.spinner.js",
                "~/Scripts/ace/elements.treeview.js",
                "~/Scripts/ace/elements.wizard.js",
                "~/Scripts/ace/elements.aside.js",
                "~/Scripts/ace/ace.js",
                "~/Scripts/ace/ace.ajax-content.js",
                "~/Scripts/ace/ace.touch-drag.js",
                "~/Scripts/ace/ace.sidebar.js",
                "~/Scripts/ace/ace.sidebar-scroll-1.js",
                "~/Scripts/ace/ace.submenu-hover.js",
                "~/Scripts/ace/ace.widget-box.js",
                "~/Scripts/ace/ace.settings.js",
                "~/Scripts/ace/ace.settings-rtl.js",
                "~/Scripts/ace/ace.settings-skin.js",
                "~/Scripts/ace/ace.widget-on-reload.js",
                "~/Scripts/ace/ace.searchbox-autocomplete.js"));

            bundles.Add(new Bundle("~/bundles/RiskAssessment").Include("~/Scripts/Areas/RiskAssessment/RiskAssessment.js"));
            bundles.Add(new Bundle("~/bundles/SelfRiskAssessment").Include("~/Scripts/Areas/RiskAssessment/SelfRiskAssessment.js"));
            bundles.Add(new Bundle("~/bundles/RiskReviewBoard").Include("~/Scripts/Areas/RiskReviewBoard/RiskReviewBoard.js"));
            bundles.Add(new Bundle("~/bundles/HIPAA_Alert").Include("~/Scripts/HIPAA/HIPAA.Alert.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}