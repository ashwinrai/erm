﻿function DisplayLeaveRequests(FunctionName) {

    var oTable = $('#TableList').dataTable({
        "bServerSide": true,
        "sAjaxSource": "../api/SelfRiskAssessmentDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": FunctionName })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 15,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
              {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },             
               {
                   "sName": "RiskType",
                   "bSearchable": false,
                   "bSortable": false,
                   "bVisible": false
               },
                 {
                     "sName": "SINo",
                     "bSearchable": false,
                     "bSortable": false,                    
                      "sWidth": "5%"
                 },
              {
                  "sName": "Questions",
                  //"sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true,
                  "sWidth": "85%",
              },
              {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {

                      return '<div class="hidden-sm hidden-xs action-buttons">' +
                             '<a class="green" href="#" onClick="editSelfRiskAssessmentQuestion_Window(\'' + oObj[0] + '\')"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                                    '<a class="red" href="#" onClick="deleteSelfRiskAssessmentQuestion_Window(\'' + oObj[0] + '\',\'' + oObj[1] + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>'

                  }//end mRender function
              }
        ]
    });
}


function addSelfRiskAssessmentQuestion_Window(FunctionName) {

    var url = window.AddSelfAssessmentQuestionsUrl + "?FunctionName=" + FunctionName;

    $("#addSelfRiskquestion").dialog({
        autoOpen: false,
        position: { my: "center", at: "top+100" },
        width: 1000,
        resizable: false,
        title: '',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('#addSelfRiskquestion').css('overflow', 'hidden');
        }
    });

    $('#addSelfRiskquestion').dialog('open');

    return false;
}


function editSelfRiskAssessmentQuestion_Window(Id) {    
    var url = "../SelfRiskAssessment/_editSelfRiskAssessmentQuestion?id=" + Id;
    $("#editSelfRiskquestion").dialog({
        position: { my: "center", at: "top+100" },
        autoOpen: false,
        width: 1000,
        resizable: false,
        title: '',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('#editSelfRiskquestion').css('overflow', 'hidden');
        }
    });

    $('#editSelfRiskquestion').dialog('open');
}

function deleteSelfRiskAssessmentQuestion_Window(Id, FunctionName) {


    var divMessage = '<div class="form-group"><p>' + 'Would you like to delete <b> Questions</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
        //title_html: true,
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "delete",
                    url: "../api/SelfRiskAssessmentDetails?id=" + Id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        DisplayLeaveRequests(FunctionName);
                        HIPAA.Alert("Self RiskAssessment Questions deleted successfully", "success");

                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("You cannot delete the existing Self RiskAssessment", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);
}