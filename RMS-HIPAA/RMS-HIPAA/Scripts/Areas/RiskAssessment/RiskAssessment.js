﻿
$("#TargetDate").val('');
$("#ClosedDate").val('');

$('#closeBtn').click(function (e) {
    $("#viewRisk").dialog("close");
});




$("#AssessedDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).next().on('click', function () {
    $(this).prev().focus();
});

$("#TargetDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).next().on('click', function () {
    $(this).prev().focus();
});

$("#ClosedDate").datepicker({
    autoclose: true,
    todayHighlight: true
}).next().on('click', function () {
    $(this).prev().focus();
});

$("#Status").change(function () {
    if ($("#Status").val() == "Close") {
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        if (month < 10) month = "0" + month;
        if (day < 10) day = "0" + day;
        var today = month + "/" + day + "/" + year;
        $("#ClosedDate").val(today);
    }
    else {
        $("#ClosedDate").val("");
    }
});


$('.chosen-select').chosen({ allow_single_deselect: true });

$("#TargetDate").change(function () {
    if ($("#TargetDate").val() != '') {
        var date = $("#TargetDate").val().split("/");
        if (date[0] > 1 && date[0] <= 3) {
            $("#TargetQurter").val("Q1");
            $('#TargetQurter').trigger("chosen:updated");
        }
        else if (date[0] > 3 && date[0] <= 6) {
            $("#TargetQurter").val("Q2");
            $('#TargetQurter').trigger("chosen:updated");
        }
        else if (date[0] > 6 && date[0] <= 9) {
            $("#TargetQurter").val("Q3");
            $('#TargetQurter').trigger("chosen:updated");
        }
        else if (date[0] > 9 && date[0] <= 12) {
            $("#TargetQurter").val("Q4");
            $('#TargetQurter').trigger("chosen:updated");
        }
       
    }
    else{
        $("#TargetQurter").val("");
        $('#TargetQurter').trigger("chosen:updated");
    }
    
    
});


$(window)
.off('resize.chosen')
.on('resize.chosen', function () {
    $('.chosen-select').each(function () {
        var $this = $(this);
        $this.next().css({ 'width': $this.parent().width() });
    });
}).trigger('resize.chosen');


$("#ProbabilityofOccurence").change(function () {
 
    var prob = $("#ProbabilityofOccurence").val() === "" ? 0 : $("#ProbabilityofOccurence").val();
    var probText = $('#ProbabilityofOccurence option:selected').text();
    $("#ProbabilityofOccurenceValue").val(probText);
    $("#Probabilit option:contains(" + probText + ")").attr('selected', 'selected');
    var impact = $("#ImpactLevel").val() === "" ? 0 : $("#ImpactLevel").val();   
    Risk = prob * impact;   
    RiskExposureLevel(Risk);
});

$("#ImpactLevel").change(function () {
    var prob = $("#ProbabilityofOccurence").val() === "" ? 0 : $("#ProbabilityofOccurence").val();
    var impact = $("#ImpactLevel").val() === "" ? 0 : $("#ImpactLevel").val();
    var impactText = $('#ImpactLevel option:selected').text();
    $("#ImpactLevelValue").val(impactText);
    $("#Impact option:contains(" + impactText + ")").attr('selected', 'selected');
    Risk = prob * impact;
    RiskExposureLevel(Risk);
   
});

$("#PIRProbabilityOfOccurence").change(function () {

    var prob = $("#PIRProbabilityOfOccurence").val() === "" ? 0 : $("#PIRProbabilityOfOccurence").val();
    var probText = $('#PIRProbabilityOfOccurence option:selected').text();
    $("#PIRProbabilityofOccurenceValue").val(probText);
    //$("#Probabilit option:contains(" + probText + ")").attr('selected', 'selected');
    var impact = $("#PIRImpactLevel").val() === "" ? 0 : $("#PIRImpactLevel").val();
    Risk = prob * impact;
    RiskExposureLevelPIR(Risk);
});

$("#PIRImpactLevel").change(function () {
    var prob = $("#PIRProbabilityOfOccurence").val() === "" ? 0 : $("#PIRProbabilityOfOccurence").val();
    var impact = $("#PIRImpactLevel").val() === "" ? 0 : $("#PIRImpactLevel").val();
    var impactText = $('#PIRImpactLevel option:selected').text();
    $("#PIRImpactLevelValue").val(impactText);
    //$("#Impact option:contains(" + impactText + ")").attr('selected', 'selected');
    Risk = prob * impact;
    RiskExposureLevelPIR(Risk);
    
});

function RiskExposureLevel(level)
{
   
   
    if(level>0&&level<=10)
    {
        $("#defaultCondition").hide();
        $("#lowCondition").show();
        $("#mediumCondition").hide();
        $("#highCondition").hide();
        $("#RiskExposureLevel").val("Low");
        $('.tbHigh').removeClass('errorClass');
        $('#TargetQurter_chosen').removeClass('errorClass');
        $('#ResponsibleDepartment_chosen').removeClass('errorClass');
        $('#RiskResponse_chosen').removeClass('errorClass');
        $('#Status_chosen').removeClass('errorClass');
        $("#RiskResponse").val("Accept");
        $('#RiskResponse').trigger("chosen:updated");
        $("#Status").val("Open");
        $('#Status').trigger("chosen:updated");   

    }
    else if (level > 10 && level <= 50) {
       
        $("#defaultCondition").hide();
        $("#lowCondition").hide();
        $("#mediumCondition").show();
        $("#highCondition").hide();
        $("#RiskExposureLevel").val("Medium");
    }
    else if (level > 50 && level <= 100) {
        
        $("#defaultCondition").hide();
        $("#lowCondition").hide();
        $("#mediumCondition").hide();
        $("#highCondition").show();
        $("#RiskExposureLevel").val("High");
    }
    else {
        
        $("#defaultCondition").show();
        $("#lowCondition").hide();
        $("#mediumCondition").hide();
        $("#highCondition").hide();
        $("#RiskExposureLevel").val(null);
    }

}

function RiskExposureLevelPIR(level) {


    if (level > 0 && level <= 10) {
        $("#defaultConditionPIR").hide();
        $("#lowConditionPIR").show();
        $("#mediumConditionPIR").hide();
        $("#highConditionPIR").hide();
        $("#PIRRiskExposureLevel").val("Low");  
        
        }
    else if (level > 10 && level <= 50) {

        $("#defaultConditionPIR").hide();
        $("#lowConditionPIR").hide();
        $("#mediumConditionPIR").show();
        $("#highConditionPIR").hide();
        $("#PIRRiskExposureLevel").val("Medium");
    }
    else if (level > 50 && level <= 100) {

        $("#defaultConditionPIR").hide();
        $("#lowConditionPIR").hide();
        $("#mediumConditionPIR").hide();
        $("#highConditionPIR").show();
        $("#PIRRiskExposureLevel").val("High");
    }
    else {

        $("#defaultConditionPIR").show();
        $("#lowConditionPIR").hide();
        $("#mediumConditionPIR").hide();
        $("#highConditionPIR").hide();
        $("#PIRRiskExposureLevel").val(null);
    }

}

function DisplayLeaveRequests(FunctionName, FunctionType) {
   
    var TableType = "Edit";
    var oTable = $('#TableList').dataTable({
        "bServerSide": true,
        "sAjaxSource": "../api/RiskAssessmentDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": FunctionName }, { "name": "TableType", "value": TableType }, { "name": "FunctionType", "value": FunctionType })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 5,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {          
           
            
            if(aData[7]=="Approved")
            {
                $('td', nRow).css('color', '#006633');
                
            }
            else if (aData[7] == "NotApproved" && aData[10] == "SentToApproval")
            {
                $('td', nRow).css('color', '#003366');
            }
            else {
                $('td', nRow).css('color', '#000000')
            }
            
        },
        "aoColumns": [
              {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },
              {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskAgent",
                  "sWidth": "20%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "AssetName",
                  "sWidth": "20%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "AssessedDate",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskDescription",
                  "sWidth": "30%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {                     
                      if (oObj[8] == "Edit") {
                         
                          if (oObj[7] == "NotApproved" && oObj[10] == "NotSentToApproval") {

                              if (oObj[6] == "Administrative") {

                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                         '<a class="green" href="../RiskAssessment/EditAdministrativeRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                                                '<a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[1] + '\',\'Administrative\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>'

                              }
                              else if (oObj[6] == "Physical") {
                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                         '<a class="green" href="../RiskAssessment/EditPhysicalRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                                                '<a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[1] + '\',\'Physical\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>'
                              }
                              else {
                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                        '<a class="green" href="../RiskAssessment/EditTechnologyRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a>' +
                                               '<a class="red" href="#" onClick="deleteRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[1] + '\',\'Technology\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>'
                              }
                          } //End if not approved
                         
                          else if (oObj[7] == "Approved" && oObj[10] == "NotSentToApproval")
                          {
                             
                              if (oObj[6] == "Administrative") {

                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                         '<a class="green" href="../RiskAssessment/EditAdministrativeRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a></div>'

                              }
                              else if (oObj[6] == "Physical") {
                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                         '<a class="green" href="../RiskAssessment/EditPhysicalRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a></div>'
                              }
                              else {
                                  return '<div class="hidden-sm hidden-xs action-buttons">' +
                                        '<a class="green" href="../RiskAssessment/EditTechnologyRiskAssessment?id=' + oObj[0] + '"><i class="ace-icon fa fa-pencil bigger-120"></i></a></div>'
                              }
                          }
                          else if (oObj[7] == "NotApproved" && oObj[10] == "SentToApproval")
                          {                              
                              return '';
                          }
                      }
                      else {                         
                          return '';
                      }//End Function  Type Edit, Open,All, In-Progress
                      
                  }//end mRender function
              }
        ],
        "columnDefs": [{
            "targets": 1,//index of column starting from 0
            "data": "TargetDate", //this name should exist in your JSON response
            "render": function (data, type, full, meta) {
            //alert();
               // alert("12: " + full[12] + " 11" + full[11]);
                var dNow = new Date();
                var utc = new Date(dNow.getTime() + dNow.getTimezoneOffset() * 60000)
                var utcdate = (utc.getMonth() + 1) + '/' + utc.getDate() + '/' + utc.getFullYear();
                //alert("Utc Date: " + utcdate);
                if (full[11] != '' && full[11] <= utcdate) {
                    return '<span><i class="fa fa-flag red"></i>&nbsp;' + full[1] + '</span>';
                }
                else {
                    return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + full[1];
                }
            }
        }]
    });
}


function deleteRiskAssessment(Id, RiskId, FunctionName)
{
   
    var divMessage = '<div class="form-group"><p>' + 'Would you like to delete <b>' + RiskId + '</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';   
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
        //title_html: true,
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "delete",                    
                    url: "../api/RiskAssessmentDetails?id=" + Id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        DisplayLeaveRequests(FunctionName, "Edit");
                        HIPAA.Alert("Risk assessment Details deleted successfully", "success");
                       
                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("Failed to delete the Risk assessment details", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);
}


function viewRisks(FunctionName,TableType, FunctionType) {
    var oTable = $('#viewTableList').dataTable({
        "bServerSide": true,
        "sAjaxSource": "../api/RiskAssessmentDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": FunctionName }, { "name": "TableType", "value": TableType }, { "name": "FunctionType", "value": FunctionType })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 5,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //alert(aData[11] + aData[13])

            if (aData[11] == "Approved") {
                $('td', nRow).css('color', '#006633');

            }
            else if (aData[11] == "NotApproved" && aData[13] == "SentToApproval") {
                $('td', nRow).css('color', '#003366');
            }
            else {
                $('td', nRow).css('color', '#000000')
            }

        },
        "aoColumns": [

             {
                 "sName": "ID",
                 "bSearchable": false,
                 "bSortable": false,
                 "bVisible": false
             },
              {
                  "sName": "RiskId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Department",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "RiskDescription",
                   "sWidth": "20%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": " ThreatVulnerabilityDescription",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "RiskImpactCategory",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ActionProposed",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": false
               },
               {
                   "sName": "TargetDate",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "ApprovedOn",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "Actions",
                   "bSearchable": false,
                   "bSortable": false,
                   "sWidth": "10%",
                   "mRender": function (data, type, oObj) {
                       return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="viewRiskAssessment(\'' + oObj[0] + '\',\'' + oObj[2] + '\',\'Technology\')"><i class="ace-icon fa fa-eye"></i></a></div>'
                   }

               }
        ],
        "columnDefs": [{
            "targets": 1,//index of column starting from 0
            "data": "TargetDate", //this name should exist in your JSON response
            "render": function (data, type, full, meta) {
                
                var dNow = new Date();
                var utc = new Date(dNow.getTime() + dNow.getTimezoneOffset() * 60000)
                var utcdate = (utc.getMonth() + 1) + '/' + utc.getDate() + '/' + utc.getFullYear();
               
                if ( full[8] !='' && full[8] <= utcdate)
                {                   
                    return '<span><i class="fa fa-flag red"></i>&nbsp;' + full[1] + '</span>';
                }
                else
                {                   
                    return '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + full[1];
                }
            }
        }]
    });
}



function viewRiskAssessment(Id, RiskId, FunctionName) {
    
    var url = "../RiskReviewBoard/_viewRisk?id=" + Id;
    $("#viewRisk").dialog({
        position: { my: "top", at: "top+100" },
        autoOpen: true,
        width: 1000,
        resizable: false,
        title: 'Risk Details',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            //$("#viewRisk").dialog('open');
            $('#viewRisk').css('overflow', 'hidden');
        }
    });
}

function   DisplaySoftwareInventory()
{
   
    var sTable = $('#SoftwareInventoryTable').dataTable({
        "dom": '<"toolbarSW">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/SoftwareInventoryDetails",
        
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 5,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
       
        "aoColumns": [
              {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },
              {
                  "sName": "AssetId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "AssetClass",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },              
              {
                  "sName": "ApplicationName",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Description",
                  "sWidth": "30%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Os",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Shared",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "ApplicationCriticality",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "DataClassification",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskAssessFreq",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "BusinessFunction",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "BusinessOwnerName",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "Department",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
              {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {
                      
                      return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="editSWI(\'' + oObj[0] + '\')"><i class="ace-icon fa fa-pencil"></i></a><a class="red" href="#" onClick="deleteSWI(\'' + oObj[0] + '\',\'' + oObj[1] + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>';
                     //End Function  Type Edit, Open,All, In-Progress

                  }//end mRender function
              }
        ]
    });
    $("div.toolbarSW").html('<p>&nbsp;<button class="btn btn-white btn-success btn-sm" id="addSWItem"><i class="ace-icon fa fa-floppy-o bigger-sm"></i>New Asset</button></p>');
    
$("#addSWItem").on('click', function () { 
    var url = "../Inventory/_addSWItem";
    $("#addSWInventoryItem").dialog({
        position: { my: "center", at: "top+100" },
        autoOpen: false,
        width: 1000,
        resizable: false,
        title: 'Software Asset',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('addSWInventoryItem').css('overflow', 'hidden');
        }
    });

    $('#addSWInventoryItem').dialog('open');
})
}

function   DisplayHardwareInventory()
{
   
    var hTable = $('#HardwareInventoryTable').dataTable({
        "dom": '<"toolbarHW">frtip',
        "bServerSide": true,
        "sAjaxSource": "../api/HardwareInventoryDetails",
        
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 5,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
       
        "aoColumns": [
              {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },
              {
                  "sName": "AssetId",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "AssetClass",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },              
              {
                  "sName": "SystemName",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Description",
                  "sWidth": "30%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Model",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "Type",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "HardwareCriticality",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              
              {
                  "sName": "RiskAssessFreq",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "BusinessFunction",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "BusinessOwnerName",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "Department",
                   "sWidth": "10%",
                   "bSearchable": true,
                   "bSortable": true
               },
              {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "10%",
                  "mRender": function (data, type, oObj) {
                      
                      return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="editHWI(\'' + oObj[0] + '\')"><i class="ace-icon fa fa-pencil"></i></a><a class="red" href="#" onClick="deleteHWI(\'' + oObj[0] + '\',\'' + oObj[1] + '\')"><i class="ace-icon fa fa-trash-o bigger-130"></i></a></div>';
                     //End Function  Type Edit, Open,All, In-Progress

                  }//end mRender function
              }
        ]
    });
    $("div.toolbarHW").html('<p>&nbsp;<button class="btn btn-white btn-success btn-sm" id="addHWItem"><i class="ace-icon fa fa-floppy-o bigger-sm"></i>New Asset</button></p>');
    
    $("#addHWItem").on('click', function () {
        var url = "../Inventory/_addHWItem";
        $("#addHWInventoryItem").dialog({
            position: { my: "center", at: "top+100" },
            autoOpen: false,
            width: 1000,
            resizable: false,
            title: 'Hardware Asset',
            title_html: true,
            modal: true,
            open: function () {
                $(this).load(url);
                $('addHWInventoryItem').css('overflow', 'hidden');
            }
        });

        $('#addHWInventoryItem').dialog('open');
    })
}



function editSWI(Id) {
    var url = "../Inventory/_editSWItem?id=" + Id;
    $("#editSWInventoryItem").dialog({
        position: { my: "center", at: "top+100" },
        autoOpen: false,
        width: 1000,
        resizable: false,
        title: 'Software Asset',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('editSWInventoryItem').css('overflow', 'hidden');
        }
    });

    $('#editSWInventoryItem').dialog('open');
}

function editHWI(Id) {
    
    var url = "../Inventory/_editHWItem?id=" + Id;
    $("#editHWInventoryItem").dialog({
        position: { my: "center", at: "top+100" },
        autoOpen: false,
        width: 1000,
        resizable: false,
        title: 'Hardware Asset',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            $('editHWInventoryItem').css('overflow', 'hidden');
        }
    });

    $('#editHWInventoryItem').dialog('open');
}

function deleteHWI(id, SWId) {
    var divMessage = '<div class="form-group"><p>' + 'Would you like to delete <b>' + SWId + '</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
        //title_html: true,
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "delete",
                    url: "../api/HardwareInventoryDetails?id=" + id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        DisplayHardwareInventory();
                        HIPAA.Alert("Hardware inventory Details deleted successfully", "success");

                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("Failed to delete the Hardware inventory details", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);

}

function deleteSWI(id, SWId) {
    var divMessage = '<div class="form-group"><p>' + 'Would you like to delete <b>' + SWId + '</b> ?' + '</p></div>';
    var dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i> Alert </h4></div>';
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,
        show: {
            effect: "blind",
            duration: 500
        },
        //title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-warning'></i> " + title + " </h4></div>",
        //title_html: true,
        buttons: [{
            text: "YES",
            "class": "btn btn-primary btn-xs",
            click: function () {
                var CurrentDialog = $(this);
                $.ajax({
                    type: "delete",
                    url: "../api/SoftwareInventoryDetails?id=" + id,
                    dataType: "json",       //    if you want to return json data, enable this data type.
                    success: function (data) {
                        CurrentDialog.dialog("close");
                        DisplaySoftwareInventory();
                        HIPAA.Alert("Software inventory Details deleted successfully", "success");

                    },
                    error: function (request, status, error) {
                        CurrentDialog.dialog("close");
                        HIPAA.Alert("Failed to delete the Software inventory details", "danger");
                    }
                });
            }
        }, {
            text: "NO",
            "class": "btn btn-primary btn-xs",
            click: function () {
                $(this).dialog("close");
            }
        }]
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);

}


function DisplayPostImplementationReviewList(FunctionName) {
    
    var pirTable = $('#PostImplementationReviewTable').dataTable({
        "bServerSide": true,
        "sAjaxSource": "../api/PostImplementationReviewDetails",
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "FunctionName", "value": FunctionName })
        },
        "sAjaxDataProp": "aaData",
        "bLengthChange": false,
        "sPaginationType": "full_numbers",
        "oLanguage": { "sZeroRecords": "No requests available", "sEmptyTable": "No requests available" },
        "bProcessing": false,
        "bDeferRender": true,
        "iDisplayLength": 5,
        "bLengthChange": false,
        "bDestroy": true,
        "bFilter": true,
        "aoColumns": [
              {
                  "sName": "ID",
                  "bSearchable": false,
                  "bSortable": false,
                  "bVisible": false
              },
              {
                  "sName": "RiskId",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "RiskDescription",
                  "sWidth": "20%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "ActionProposed",
                  "sWidth": "10%",
                  "bSearchable": true,
                  "bSortable": true
              },
              {
                  "sName": "ProbabilityofOccurence",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "ImpactLevel",
                   "sWidth": "5%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "RiskExposureLevel",
                   "sWidth": "5%",
                   "bSearchable": true,
                   "bSortable": true
               },
              {
                  "sName": "PIRProbabilityOfOccurence",
                  "sWidth": "5%",
                  "bSearchable": true,
                  "bSortable": true
              },
               {
                   "sName": "PIRImpactLevel",
                   "sWidth": "5%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "PIRRiskExposureLevel",
                   "sWidth": "5%",
                   "bSearchable": true,
                   "bSortable": true
               },
               {
                   "sName": "PIRComments",
                   "sWidth": "5%",
                   "bSearchable": true,
                   "bSortable": true
               },
              {
                  "sName": "Actions",
                  "bSearchable": false,
                  "bSortable": false,
                  "sWidth": "5%",
                  "mRender": function (data, type, oObj) {
                      return '<div class="hidden-sm hidden-xs action-buttons"><a class="green" href="#" onClick="performPIR(\'' + oObj[0] + '\',\'' + oObj[1] + '\',\'Technology\')"><i class="ace-icon fa  fa-arrow-circle-o-down"></i></a></div>'
                  }//end mRender function
              }
        ]
    });
}
function performPIR(Id, RiskId, FunctionName) {
    
    var url = "../PostImplementationRevew/_performPostImplementationReview?id=" + Id;
    $("#performPostImplementationReview").dialog({
        position: { my: "top", at: "top+100" },
        autoOpen: true,
        width: 1000,
        resizable: false,
        title: 'Risk Details',
        title_html: true,
        modal: true,
        open: function () {
            $(this).load(url);
            //$("#viewRisk").dialog('open');
            $('#performPostImplementationReview').css('overflow', 'hidden');
        }
    });

}