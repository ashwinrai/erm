﻿var HIPAA = HIPAA || {};

//HIPAA.Alert = function (message, status) {
//    $(function () {
//        setTimeout(function () {
//            $.bootstrapGrowl(message, {
//                type: status,
//                align: 'center'
//            });
//        }, 1000);
//    });
//};

HIPAA.Alert = function (message, status) {
    var divMessage = '<div class="form-group"><p><br/>'+ message +'</p></div>';   
    var dlgtitle;
        if (status == "success") {
            dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-check"></i>&nbsp;Success !</h4></div>';
        }
        else if (status = "danger") {
            dlgtitle = '<div class="widget-header widget-header-small"><h4 class="smaller"><i class="ace-icon fa fa-warning"></i>&nbsp;Failure !</h4></div>';
        }
    
    
    $(divMessage).dialog({
        modal: true,
        autoOpen: true,        
        height:"auto",
        show: {
            effect: "blind",
            duration: 100
        },
        open: function (eve, ui) {
            var item = $(this);
            window.setTimeout(function () {
                item.dialog('close');
            },
            3000);
        }
    }).siblings('.ui-dialog-titlebar').html(dlgtitle);
};

//HIPAA.Alert = function (message, status) {

//    if (status == "success") {
//        $().toastmessage("showSuccessToast", message);
//    }
//    else if (status = "danger") {
//        $().toastmessage("showErrorToast", message);
//    }
//    else if (status = "notice") {
//        $().toastmessage("showNoticeToast", message);
//    }
//    else {
//        $().toastmessage("showWarningToast", message);
//    }
//};

function showStickySuccessToast() {
    $().toastmessage('showToast', {
        text: 'Success Dialog which is sticky',
        sticky: true,
        position: 'top-center',
        type: 'success',
        closeText: '',
        close: function () {
            console.log("toast is closed ...");
        }
    });

}

function showStickyNoticeToast() {
    $().toastmessage('showToast', {
        text: 'Notice Dialog which is sticky',
        sticky: true,
        position: 'top-right',
        type: 'notice',
        closeText: '',
        close: function () { console.log("toast is closed ..."); }
    });
}

function showStickyWarningToast() {
    $().toastmessage('showToast', {
        text: 'Warning Dialog which is sticky',
        sticky: true,
        position: 'top-right',
        type: 'warning',
        closeText: '',
        close: function () {
            console.log("toast is closed ...");
        }
    });
}

function showStickyErrorToast() {
    $().toastmessage('showToast', {
        text: 'Error Dialog which is sticky',
        sticky: true,
        position: 'top-right',
        type: 'error',
        closeText: '',
        close: function () {
            console.log("toast is closed ...");
        }
    });
}
